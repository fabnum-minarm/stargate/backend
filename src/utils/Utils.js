export default class Utils {
	static cleanURL(url) {
		return url.replaceAll('//', '/');
	}
}
