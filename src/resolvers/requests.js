import mongoose from 'mongoose';
import RequestModel, { EVENT_REMOVE, STATE_REMOVED } from '../models/request';
import { TEMPLATES_ROLES, VALIDATION_SCOPE_UNIT } from '../models/constants/role';
import { BUCKETNAME_CAMPUS_FILE } from '../models/campus';
import DownloadToken from '../models/download-token';

export const CampusMutation = {
  async createRequest(campus, { request, unit }, { user }) {
    request.from.setHours(7, 0, 0, 0);
    request.to.setHours(19, 0, 0, 0);
    return campus.createRequest(Object.assign(
      request,
      { owner: { ...user, unit: { _id: unit.id, label: unit.label } } },
    ));
  },
  async editRequest(campus, { request, id }) {
    const r = await RequestModel.findById(id);
    if (request.places) {
      request.places = await campus.findPlacesbyId(request.places);
    }
    r.set(request);
    return r.save();
  },

  async deleteRequest(campus, { id }) {
    const r = await RequestModel.findById(id);
    if (!r) {
      throw new Error('Request not found');
    }
    const possibleEvents = r.listPossibleEvents();
    if (possibleEvents.indexOf(EVENT_REMOVE) === -1) {
      throw new Error('You cannot shift to this state');
    }
    await r.stateMutation(EVENT_REMOVE);
    await r.save();
    return r;
  },

  async mutateRequest(_, { id }) {
    const request = await RequestModel.findById(id);
    if (!request) {
      throw new Error('Request not found');
    }
    return request;
  },
  async shiftRequest(request, { id, transition }) {
    const r = await RequestModel.findById(id);
    if (!r) {
      throw new Error('Request not found');
    }
    const possibleEvents = r.listPossibleEvents();
    if (possibleEvents.indexOf(transition) === -1) {
      throw new Error('You cannot shift to this state');
    }
    await r.stateMutation(transition);
    return r.status === STATE_REMOVED ? r : r.save();
  },

};

const MAX_REQUESTABLE_REQUESTS = 50;
export const Campus = {
  async getRequest(_parent, { id }) {
    const request = await RequestModel.findById(id);
    if (!request) {
      throw new Error('Request not found');
    }
    return request;
  },
  async listRequests(campus, {
    as,
    filters = {},
    cursor: { offset = 0, first = MAX_REQUESTABLE_REQUESTS } = {},
    search,
    sort = { from: 'ascending' },
  }) {
    const roleData = TEMPLATES_ROLES.find((r) => r.template === as.role);
    const roleFilters = { 'units.workflow.steps.role.template': roleData.template };
    const unitFilters = roleData.validator.scope === VALIDATION_SCOPE_UNIT
      ? { 'units.label': as.unit }
      : {};
    const searchFilters = {};
    if (search) {
      searchFilters.$text = { $search: search };
    }
    return {
      campus,
      filters: {
        ...filters, ...roleFilters, ...unitFilters, ...searchFilters,
      },
      cursor: { offset, first: Math.min(first, MAX_REQUESTABLE_REQUESTS) },
      countMethod: campus.countRequests.bind(campus, {
        ...filters, ...roleFilters, ...unitFilters, ...searchFilters,
      }),
      sort,
    };
  },
  async listMyRequests(
    campus,
    {
      filters = {},
      cursor: { offset = 0, first = MAX_REQUESTABLE_REQUESTS } = {},
      search,
      sort = { from: 'ascending' },
    },
    { user },
  ) {
    const userFilters = {
      ...filters,
      'owner._id': mongoose.Types.ObjectId(user.id),
    };
    const searchFilters = {};
    if (search) {
      searchFilters.$text = { $search: search };
    }
    return {
      campus,
      filters: { ...userFilters, ...searchFilters },
      cursor: { offset, first: Math.min(first, MAX_REQUESTABLE_REQUESTS) },
      countMethod: campus.countRequests.bind(campus, { ...userFilters, ...searchFilters }),
      sort,
    };
  },
  async generateSecurityFileExportLink(campus) {
    return campus.campusDocuments[0] && campus.campusDocuments[0].file
      ? DownloadToken.createIdentityFileToken(
        BUCKETNAME_CAMPUS_FILE,
        campus.campusDocuments[0].file,
      )
      : null;
  },
  async generateAccesPlanExportLink(campus) {
    return campus.campusDocuments[1] && campus.campusDocuments[1].file
      ? DownloadToken.createIdentityFileToken(
        BUCKETNAME_CAMPUS_FILE,
        campus.campusDocuments[1].file,
      )
      : null;
  },
};

export const RequestsList = {
  async list(
    {
      campus, filters, cursor: { offset, first }, sort,
    },
    _params,
    _ctx,
    info,
  ) {
    return campus.findRequestsWithProjection(filters, info).skip(offset).limit(first).sort(sort);
  },
  meta: (parent) => parent,
};
