import Campus, { BUCKETNAME_CAMPUS_FILE } from '../models/campus';
import { INIT_CAMPUS_ROLES, TEMPLATES_ROLES } from '../models/constants/role';

export const Mutation = {
  async createCampus(_, { id, campus }) {
    return Campus.create({ id, roles: INIT_CAMPUS_ROLES, ...campus });
  },
  async editCampus(_, { campus, id }) {
    const c = await Campus.findById(id);
    if (!c) {
      throw new Error('Campus not found');
    }
    let datas = campus;
    if (campus.file) {
      const file = await c.uploadSecurityFile(
        campus, BUCKETNAME_CAMPUS_FILE,
      );
      if (file === 'File upload error') {
        throw new Error('File upload error');
      }

      const newCampusDocs = c.campusDocuments;
      newCampusDocs[parseInt(campus.fileIndexChanged, 10)] = { file };

      datas = {
        ...campus,
        campusDocuments: newCampusDocs,
      };
    }
    c.set(datas);
    return c.save();
  },
  async mutateCampus(_, { id }) {
    return Campus.findById(id);
  },
  async deleteCampus(_, { id }) {
    const removedCampus = await Campus.findByIdAndRemove(id);
    if (!removedCampus) {
      throw new Error('Campus not found');
    }
    return removedCampus;
  },
  async addCampusRole(_, { id, role }) {
    const campus = await Campus.findById(id);
    if (!campus) {
      throw new Error('Campus not found');
    }
    const roleTemplate = TEMPLATES_ROLES.find((t) => t.template === role.template);
    campus.roles.push({ ...roleTemplate, ...role });
    return campus.save();
  },
  async editCampusRole(_, {
    id, shortLabel, role, tag,
  }) {
    const campus = await Campus.findById(id);
    if (!campus) {
      throw new Error('Campus not found');
    }
    const roleToAdd = { ...TEMPLATES_ROLES.find((r) => r.template === role.template), ...role };
    campus.roles = campus.roles.map((r) => {
      if (r.shortLabel === shortLabel) {
        if (tag) {
          const indexToInsert = roleToAdd.tags.findIndex((e) => e.primary === false);
          roleToAdd.tags.splice(indexToInsert, 0, tag);
          return roleToAdd;
        }
        return roleToAdd;
      }
      return r;
    });
    await campus.save();
    await campus.editCampusRoleDependencies(roleToAdd);
    return campus;
  },

};

const MAX_REQUESTABLE_CAMPUSES = 30;
export const Query = {
  async listCampuses(_parent, { filters = {}, cursor: { offset = 0, first = MAX_REQUESTABLE_CAMPUSES } = {}, search }) {
    let searchFilters = {};
    if (search) {
      searchFilters = { $or: [{ label: { $regex: search, $options: 'i' } }] };
    }
    return {
      filters: { ...filters, ...searchFilters },
      cursor: { offset, first: Math.min(first, MAX_REQUESTABLE_CAMPUSES) },
      countMethod: Campus.countDocuments.bind(Campus),
    };
  },
  async getCampus(_parent, { id }, _ctx, info) {
    return Campus.findByIdWithProjection(id, info);
  },
};

export const CampusesList = {
  async list({ filters, cursor: { offset, first } }, _params, _ctx, info) {
    return Campus.findWithProjection(filters, info).skip(offset).limit(first);
  },
  meta: (parent) => parent,
};
