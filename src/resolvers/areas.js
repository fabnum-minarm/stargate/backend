import Area from '../models/area';

export const Mutation = {
  async createArea(_, { area }) {
    const campuses = await Area.findCampusesById(area.campuses);
    return Area.create({ label: area.label, campuses });
  },
  async editArea(_, { area, id }) {
    const a = await Area.findById(id);
    if (!a) {
      throw new Error('Area not found');
    }
    const campuses = await Area.findCampusesById(area.campuses);
    a.set({ label: area.label, campuses });
    return a.save();
  },
  async deleteArea(_, { id }) {
    const removedArea = await Area.findByIdAndRemove(id);
    if (!removedArea) {
      throw new Error('Area not found');
    }
    return removedArea;
  },
};

const MAX_REQUESTABLE_AREAS = 30;
export const Query = {
  async listAreas(_parent, { filters = {}, cursor: { offset = 0, first = MAX_REQUESTABLE_AREAS } = {} }) {
    return {
      filters,
      cursor: { offset, first: Math.min(first, MAX_REQUESTABLE_AREAS) },
      countMethod: Area.countDocuments.bind(Area),
    };
  },
  async listCampusAreas(
    _parent,
    { filters = {}, cursor: { offset = 0, first = MAX_REQUESTABLE_AREAS } = {}, campusId },
  ) {
    const campusFilter = { 'campuses._id': campusId };
    return {
      filters: { ...filters, ...campusFilter },
      cursor: { offset, first: Math.min(first, MAX_REQUESTABLE_AREAS) },
      countMethod: Area.countDocuments.bind(Area),
    };
  },
  async getArea(_parent, { id }, _ctx, info) {
    return Area.findByIdWithProjection(id, info);
  },
};

export const AreasList = {
  async list({ filters, cursor: { offset, first } }, _params, _ctx, info) {
    return Area.findWithProjection(filters, info).skip(offset).limit(first);
  },
  meta: (parent) => parent,
};
