import EmployeeType from '../models/employeetype';

export const CampusMutation = {
  async createEmployeeType(campus, { employeetype }) {
    return campus.createEmployeeType(employeetype);
  },
  async editEmployeeType(campus, { employeetype, id }) {
    const c = await EmployeeType.findById(id);
    c.set(employeetype);
    return c.save();
  },
  async deleteEmployeeType(campus, { id }) {
    const removedEmployeeType = await campus.findEmployeeTypeByIdAndRemove(id);
    if (!removedEmployeeType) {
      throw new Error('EmployeeType not found');
    }
    return removedEmployeeType;
  },
};

const MAX_REQUESTABLE_EMPLOYEETYPE = 30;
export const Campus = {
  async listEmployeeType(campus, { filters = {}, cursor: { offset = 0, first = MAX_REQUESTABLE_EMPLOYEETYPE } = {}, search }) {
    let searchFilters = {};
    if (search) {
      searchFilters = { $or: [{ label: { $regex: search, $options: 'i' } }] };
    }
    return {
      campus,
      filters: { ...filters, ...searchFilters },
      cursor: { offset, first: Math.min(first, MAX_REQUESTABLE_EMPLOYEETYPE) },
      countMethod: campus.countEmployeeType.bind(campus),
    };
  },
  async getEmployeeType(_parent, { id }, _ctx, info) {
    return EmployeeType.findByIdWithProjection(id, info);
  },
};

export const EmployeeTypeList = {
  async list({ campus, filters, cursor: { offset, first } }, _params, _ctx, info) {
    return campus.findEmployeeTypeWithProjection(filters, info).skip(offset).limit(first);
  },
  meta: (parent) => parent,
};
