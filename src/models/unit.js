import mongoose from 'mongoose';

import { VALIDATION_SCOPE_ENUM } from './constants/role';
import { PLACE_MODEL_NAME, UNIT_MODEL_NAME, USER_MODEL_NAME } from './constants/modelNames';
import { WORKFLOW_ENUM } from './constants/unit';

const { Schema } = mongoose;

const UnitSchema = new Schema({
  label: { type: String, required: true },
  trigram: { type: String, required: true },
  campus: {
    _id: String,
    label: String,
    timezone: String,
  },
  workflow: {
    steps: [
      {
        role: {
          template: { type: String, required: true },
          label: String,
          shortLabel: String,
          validator: {
            scope: { type: String, enum: VALIDATION_SCOPE_ENUM },
            behavior: {
              type: String,
              enum: WORKFLOW_ENUM,
            },
          },
        },
        condition: {
          role: String,
          value: Boolean,
        },
      },
    ],
  },
}, { timestamps: true });

UnitSchema.post('save', async (unit) => {
  await unit.editUnitDependencies();
});

UnitSchema.methods.editUnitDependencies = async function deleteUnitDependencies() {
  const Place = mongoose.model(PLACE_MODEL_NAME);
  const User = mongoose.model(USER_MODEL_NAME);
  await Place.updateMany({ 'unitInCharge._id': this._id }, { unitInCharge: this });
  await User.updateMany(
    { 'roles.units._id': this._id },
    { $set: { 'roles.$[].units.$[unit]': this } },
    { arrayFilters: [{ 'unit._id': this._id }], multi: true },
  );
  return this;
};

UnitSchema.methods.deleteUnitDependencies = async function deleteUnitDependencies() {
  const Place = mongoose.model(PLACE_MODEL_NAME);
  const User = mongoose.model(USER_MODEL_NAME);
  const unitPlaces = await Place.find({ 'unitInCharge._id': this._id });
  const unitUsers = await User.find({ 'roles.units._id': this._id });

  if (unitPlaces) {
    await Promise.all(unitPlaces.map(async (place) => {
      place.set('unitInCharge', null, { strict: false });
      return place.save();
    }));
  }
  if (unitUsers) {
    await Promise.all(unitUsers.map(async (user) => user.roles.map((role) => {
      if (role.units.find((unit) => unit._id.equals(this._id))) {
        return user.deleteUserRole({ role: role.role.template, unit: this });
      }
      return role;
    })));
  }
  return this;
};

export default mongoose.model(UNIT_MODEL_NAME, UnitSchema, 'units');
