import mongoose from 'mongoose';
import { AREA_MODEL_NAME, CAMPUS_MODEL_NAME, USER_MODEL_NAME } from './constants/modelNames';

const { Schema } = mongoose;

const AreaSchema = new Schema({
  label: { type: String, required: true },
  campuses: [{
    _id: { type: String, alias: 'id' },
    label: String,
    trigram: String,
  }],
}, { timestamps: true });

AreaSchema.post('save', async (area) => {
  await area.editAreaDependencies();
});

AreaSchema.statics.findCampusById = async function findCampusById(id) {
  const Campus = mongoose.model(CAMPUS_MODEL_NAME);
  return Campus.findById(id);
};

AreaSchema.statics.findCampusesById = async function findCampusesById(campusesId) {
  const campuses = await Promise.all(campusesId.map((campusId) => this.findCampusById(campusId)));
  return campuses;
};

AreaSchema.methods.editAreaDependencies = async function editAreaDependencies() {
  const User = mongoose.model(USER_MODEL_NAME);
  await User.updateMany(
    { 'roles.area._id': this._id },
    { $set: { 'roles.$[role].area': this, 'roles.$[role].campuses': this.campuses } },
    { arrayFilters: [{ 'role.area._id': this._id }], multi: true },
  );
};

export default mongoose.model(AREA_MODEL_NAME, AreaSchema, 'areas');
