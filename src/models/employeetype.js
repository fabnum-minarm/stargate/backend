import mongoose from 'mongoose';
import { EMPLOYEE_TYPE_MODEL_NAME } from './constants/modelNames';

const { Schema } = mongoose;

const EmployeeTypeSchema = new Schema({
  label: { type: String, required: true },
  campus: {
    _id: String,
    label: String,
    timezone: String,
  },
}, { timestamps: true });

export default mongoose.model(EMPLOYEE_TYPE_MODEL_NAME, EmployeeTypeSchema, 'employeetype');
