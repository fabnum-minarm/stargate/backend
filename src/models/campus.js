import mongoose from 'mongoose';
import timezoneValidator from 'timezone-validator';
import {
  WORKFLOW_BEHAVIOR_VALIDATION,
  WORKFLOW_DECISION_ACCEPTED, WORKFLOW_DECISION_NEGATIVE,
  WORKFLOW_DECISION_POSITIVE, WORKFLOW_ENUM,
} from './constants/unit';
import {
  STATE_CANCELED,
  STATE_CREATED,
  STATE_REJECTED,
} from './request';

import { uploadFile } from './helpers/upload';

import {
  EXPORT_XLSX_TEMPLATE_VISITORS,
  EXPORT_CSV_VISITORS,
  ID_DOCUMENT_IDCARD,
  ID_DOCUMENT_PASSPORT,
  ID_DOCUMENT_CIMSCARD,
} from './visitor';
import ExportToken from './export-token';
import config from '../services/config';

import {
  CAMPUS_MODEL_NAME,
  ZONE_MODEL_NAME,
  PLACE_MODEL_NAME,
  USER_MODEL_NAME,
  ROLE_MODEL_NAME,
  VISITOR_MODEL_NAME,
  REQUEST_MODEL_NAME,
  UNIT_MODEL_NAME, EMPLOYEE_TYPE_MODEL_NAME,
} from './constants/modelNames';
import { PERMISSIONS_ENUM, VALIDATION_SCOPE_ENUM } from './constants/role';

const DEFAULT_TIMEZONE = config.get('default_timezone');
const { Schema } = mongoose;

export const BUCKETNAME_CAMPUS_FILE = 'campusFile';

const CampusSchema = new Schema({
  _id: { type: String, alias: 'id' },
  label: { type: String, required: true },
  trigram: { type: String, required: true },
  securityNotificationEmail: { type: String },
  securityNotificationEnabled: { type: Boolean, required: true, default: false },
  timezone: {
    type: String,
    default: process.env.TZ || DEFAULT_TIMEZONE,
    validate: {
      validator(v) {
        return !v || timezoneValidator(v);
      },
      message({ value }) {
        return `"${value}" seems to don't be a valid timezone`;
      },
    },
  },
  roles: [{
    template: { type: String, required: true },
    label: { type: String, required: true },
    shortLabel: { type: String, required: true },
    validator: {
      scope: { type: String, enum: VALIDATION_SCOPE_ENUM },
      behavior: {
        type: String,
        enum: WORKFLOW_ENUM,
      },
    },
    tags: [{
      label: String,
      value: String,
      primary: { type: Boolean, default: false },
    }],
    permissions: [{ type: String, enum: PERMISSIONS_ENUM }],
  }],
  campusDocuments: [{
    file: {
      _id: {
        type: Schema.ObjectId,
        alias: 'file.id',
      },
      filename: String,
    },
  }],
}, { timestamps: true });

CampusSchema.post('save', async (campus) => {
  await campus.editCampusDependencies();
});

CampusSchema.methods.editCampusDependencies = async function editCampusDependencies() {
  const modelsToUpdate = [
    { modelName: PLACE_MODEL_NAME, field: 'campus' },
    { modelName: UNIT_MODEL_NAME, field: 'campus' },
    { modelName: REQUEST_MODEL_NAME, field: 'campus' },
    { modelName: VISITOR_MODEL_NAME, field: 'request.campus' },
    { modelName: EMPLOYEE_TYPE_MODEL_NAME, field: 'campus' },
  ];
  await Promise.all(modelsToUpdate.map(async (m) => {
    const Model = mongoose.model(m.modelName);
    return Model.updateMany({ [`${m.field}._id`]: this._id }, { [m.field]: this });
  }));

  const User = mongoose.model(USER_MODEL_NAME);
  await User.updateMany(
    { 'roles.campuses._id': this._id },
    { $set: { 'roles.$[].campuses.$[campus]': this } },
    { arrayFilters: [{ 'campus._id': this._id }], multi: true },
  );
};

CampusSchema.methods.editCampusRoleDependencies = async function editCampusRoleDependencies(role) {
  const Unit = mongoose.model(UNIT_MODEL_NAME);
  const User = mongoose.model(USER_MODEL_NAME);
  await Unit.updateMany(
    { 'campus._id': this._id, 'workflow.steps.role.template': role.template },
    { $set: { 'workflow.steps.$[step].role': role } },
    { arrayFilters: [{ 'step.role.template': role.template }], multi: true },
  );
  await User.updateMany(
    { roles: { $elemMatch: { 'role.template': role.template, 'campuses._id': this._id } } },
    { $set: { 'roles.$[r].role': role } },
    { arrayFilters: [{ 'r.role.template': role.template, 'r.campuses._id': this._id }], multi: true },
  );
};

CampusSchema.methods.createUnit = async function createUnit(data) {
  const Unit = mongoose.model(UNIT_MODEL_NAME);
  const unit = new Unit(data);
  unit.campus = this;
  return unit.save();
};

CampusSchema.methods.createZone = async function createZone(data) {
  const Zone = mongoose.model(ZONE_MODEL_NAME);
  const zone = new Zone(data);
  zone.campus = this;
  return zone.save();
};

CampusSchema.methods.createEmployeeType = async function createEmployeeType(data) {
  const EmployeeType = mongoose.model(EMPLOYEE_TYPE_MODEL_NAME);
  const employeetype = new EmployeeType(data);
  employeetype.campus = this;
  return employeetype.save();
};

CampusSchema.methods.createPlaceFromGraphQLSchema = async function createPlace(data) {
  const Place = mongoose.model(PLACE_MODEL_NAME);
  const place = new Place();
  place.campus = this;
  await place.setFromGraphQLSchema(data);
  return place.save();
};

CampusSchema.methods.findUnitsWithProjection = function findUnitsWithProjection(filters, ...params) {
  const Unit = mongoose.model(UNIT_MODEL_NAME);
  return Unit.findWithProjection({ ...filters, 'campus._id': this._id }, ...params);
};

CampusSchema.methods.countUnits = async function countUnits(filters) {
  const Unit = mongoose.model(UNIT_MODEL_NAME);
  return Unit.countDocuments({ ...filters, 'campus._id': this._id });
};

CampusSchema.methods.findRolesWithProjection = function findRolesWithProjection(filters, ...params) {
  const Role = mongoose.model(ROLE_MODEL_NAME);
  return Role.findWithProjection({ ...filters, 'campuses._id': this._id }, ...params);
};

CampusSchema.methods.countRoles = async function countRoles(filters) {
  const Role = mongoose.model(ROLE_MODEL_NAME);
  return Role.countDocuments({ ...filters, 'campuses._id': this._id });
};

CampusSchema.methods.findUnitbyId = async function findUnitbyId(id) {
  const Unit = mongoose.model(UNIT_MODEL_NAME);
  return Unit.findOne({ _id: id, 'campus._id': this._id });
};

CampusSchema.methods.findZonesWithProjection = function findZonesWithProjection(filters, ...params) {
  const Zone = mongoose.model(ZONE_MODEL_NAME);
  return Zone.findWithProjection({ ...filters, 'campus._id': this._id }, ...params);
};

CampusSchema.methods.countZones = async function countZones(filters) {
  const Zone = mongoose.model(ZONE_MODEL_NAME);
  return Zone.countDocuments({ ...filters, 'campus._id': this._id });
};

CampusSchema.methods.findZonebyId = async function findZonebyId(id) {
  const Zone = mongoose.model(ZONE_MODEL_NAME);
  return Zone.findOne({ _id: id, 'campus._id': this._id });
};

CampusSchema.methods.findEmployeeTypeById = async function findEmployeeTypeById(id) {
  const EmployeeType = mongoose.model(EMPLOYEE_TYPE_MODEL_NAME);
  return EmployeeType.findOne({ _id: id, 'campus._id': this._id });
};

CampusSchema.methods.findEmployeeTypeWithProjection = function findEmployeeTypeWithProjection(filters, ...params) {
  const EmployeeType = mongoose.model(EMPLOYEE_TYPE_MODEL_NAME);
  return EmployeeType.findWithProjection({ ...filters, 'campus._id': this._id }, ...params);
};

CampusSchema.methods.countEmployeeType = async function countEmployeeType(filters) {
  const EmployeeType = mongoose.model(EMPLOYEE_TYPE_MODEL_NAME);
  return EmployeeType.countDocuments({ ...filters, 'campus._id': this._id });
};

CampusSchema.methods.findPlacesWithProjection = function findPlacesWithProjection(filters, ...params) {
  const Place = mongoose.model(PLACE_MODEL_NAME);
  return Place.findWithProjection({ ...filters, 'campus._id': this._id }, ...params);
};

CampusSchema.methods.countPlaces = async function countPlaces(filters) {
  const Place = mongoose.model(PLACE_MODEL_NAME);
  return Place.countDocuments({ ...filters, 'campus._id': this._id });
};

CampusSchema.methods.findPlacebyId = async function findPlacebyId(id) {
  const Place = mongoose.model(PLACE_MODEL_NAME);
  return Place.findOne({ _id: id, 'campus._id': this._id });
};

CampusSchema.methods.findPlacesbyId = async function findPlacesbyId(placesId) {
  const places = await Promise.all(placesId.map((placeId) => this.findPlacebyId(placeId)));
  return places;
};

CampusSchema.methods.findZoneByIdAndRemove = async function findZoneByIdAndRemove(id) {
  const Zone = mongoose.model(ZONE_MODEL_NAME);
  return Zone.findOneAndRemove({ _id: id, 'campus._id': this._id });
};

CampusSchema.methods.findEmployeeTypeByIdAndRemove = async function findEmployeeTypeByIdAndRemove(id) {
  const EmployeeType = mongoose.model(EMPLOYEE_TYPE_MODEL_NAME);
  return EmployeeType.findOneAndRemove({ _id: id, 'campus._id': this._id });
};

CampusSchema.methods.createRequest = async function createRequest(data) {
  const Request = mongoose.model(REQUEST_MODEL_NAME);
  const request = new Request(data);
  request.campus = this;
  // @todo : find a way to separate concerns here
  request.places = await this.findPlacesbyId(data.places);
  return request.save();
};

CampusSchema.methods.findRequestsWithProjection = function findRequestsWithProjection(filters, ...params) {
  const Request = mongoose.model(REQUEST_MODEL_NAME);
  return Request.findWithProjection({ ...filters, 'campus._id': this._id }, ...params);
};

CampusSchema.methods.countRequests = async function countRequests(filters) {
  const Request = mongoose.model(REQUEST_MODEL_NAME);
  return Request.countDocuments({ ...filters, 'campus._id': this._id });
};

CampusSchema.methods.findVisitorsWithProjection = function findVisitorsWithProjection(filters, ...params) {
  const Visitor = mongoose.model(VISITOR_MODEL_NAME);
  return Visitor.findWithProjection({ ...filters, 'request.campus._id': this._id }, ...params);
};

CampusSchema.methods.countVisitors = async function countVisitors(filters) {
  const Visitor = mongoose.model(VISITOR_MODEL_NAME);
  return Visitor.countDocuments({ ...filters, 'request.campus._id': this._id });
};

CampusSchema.methods.createCSVTokenForVisitors = async function createCSVTokenForVisitors(filters, options) {
  const Visitor = mongoose.model(VISITOR_MODEL_NAME);
  const fields = EXPORT_CSV_VISITORS;
  const projection = Object.fromEntries(fields.map((f) => ([f.value, true])));
  const projformat = Object.fromEntries(fields.filter((f) => f.format).map((f) => ([f.value, f.format])));
  return ExportToken.createCSVToken(
    Visitor,
    fields,
    { ...filters, 'request.campus._id': this._id },
    projection,
    projformat,
    true,
    options,
  );
};

CampusSchema.methods.createCSVTokenForUnitsVisitors = async function createCSVTokenForUnitsVisitors(
  filters, unitsId, options,
) {
  const Visitor = mongoose.model(VISITOR_MODEL_NAME);
  const fields = EXPORT_CSV_VISITORS;
  // remove STATUT
  fields.shift();
  const projection = Object.fromEntries(fields.map((f) => ([f.value, true])));
  const projformat = Object.fromEntries(fields.filter((f) => f.format).map((f) => ([f.value, f.format])));

  const lowerBound = new Date();
  lowerBound.setHours(0, 0, 0, 0);
  const upperBound = new Date();
  upperBound.setHours(24, 0, 0, 0);

  return ExportToken.createCSVToken(
    Visitor,
    fields,
    {
      ...filters,
      'request.campus._id': this._id,
      'request.units': {
        $elemMatch: {
          _id: { $in: unitsId },
          'workflow.steps': { $not: { $elemMatch: { 'state.isOK': false } } },
        },
      },
      'request.from': {
        $gte: lowerBound,
        $lt: upperBound,
      },
    },
    projection,
    projformat,
    false,
    options,
  );
};

CampusSchema.methods.createVisitorsTemplate = async function createVisitorsTemplate() {
  return ExportToken.createXLSXToken(
    null,
    EXPORT_XLSX_TEMPLATE_VISITORS,
  );
};

CampusSchema.methods.findVisitorsToValidate = async function findVisitorsToValidate(
  { role, unit }, filters, offset, first,
) {
  const Visitor = mongoose.model(VISITOR_MODEL_NAME);

  const stateValue = { 'workflow.steps': { $elemMatch: { 'role.template': role, 'state.value': { $exists: false } } } };
  const avoidRejected = {
    'workflow.steps': {
      $not: { $elemMatch: { 'role.validator.behavior': WORKFLOW_BEHAVIOR_VALIDATION, 'state.isOK': false } },
    },
  };

  const notDoneFilter = {
    'request.units': unit
      ? { $elemMatch: { $and: [{ _id: mongoose.Types.ObjectId(unit) }, stateValue, avoidRejected] } }
      : { $elemMatch: { $and: [stateValue, avoidRejected] } },
    status: { $nin: [STATE_CANCELED] },
  };

  const nextStepsFilter = {
    $filter: {
      input: '$request.unitsToCheck.workflow.steps',
      as: 'step',
      cond: {
        $and: [
          { $ne: ['$$step.state.value', WORKFLOW_DECISION_ACCEPTED] },
          { $ne: ['$$step.state.value', WORKFLOW_DECISION_POSITIVE] },
          { $ne: ['$$step.state.value', WORKFLOW_DECISION_NEGATIVE] },
        ],
      },
    },
  };

  const visitorsListTotal = Visitor.aggregate()
    .match({ 'request.campus._id': this._id })
    .match(filters)
    .match(notDoneFilter)
    .addFields({
      'request.unitsToCheck': '$request.units',
    })
    .unwind('request.unitsToCheck')
    .match(unit ? { 'request.unitsToCheck._id': mongoose.Types.ObjectId(unit) } : {})
    .addFields({ nextSteps: nextStepsFilter })
    .addFields({ nextStep: { $arrayElemAt: ['$nextSteps', 0] } })
    .match({ 'nextStep.role.template': role })
    .group({
      _id: '$_id',
      visitor: { $mergeObjects: '$$ROOT' },
    })
    .replaceRoot('$visitor')
    .addFields({
      id: '$_id',
      'request.id': '$request._id',
      'request.units':
      {
        $map:
        {
          input: '$request.units',
          as: 'unit',
          in: {
            id: { $toString: '$$unit._id' },
            workflow: {
              steps: {
                $map:
                    {
                      input: '$$unit.workflow.steps',
                      as: 'step',
                      in: {
                        state: '$$step.state',
                        role: { $mergeObjects: ['$$step.role', { id: '$$step.role.template' }] },
                      },
                    },
              },
            },
            label: '$$unit.label',
          },
        },
      },
      identityDocuments: {
        $map: {
          input: '$identityDocuments',
          as: 'identityDocument',
          in:
            {
              $mergeObjects: [
                '$$identityDocument',
                { file: { $mergeObjects: ['$$identityDocument.file', { id: '$$identityDocument.file._id' }] } },
              ],
            },
        },
      },
    })
    .project({ _id: 0 });

  const paginateVisitors = async () => {
    const list = await visitorsListTotal
      .skip(offset)
      .limit(first);
    return list;
  };

  const countVisitors = await visitorsListTotal.exec();
  const visitorsListPaginated = await paginateVisitors();

  return { list: visitorsListPaginated, total: countVisitors.length };
};

// @todo delete method (and resolvers, schemas, test) when the front will display list of visitors instead of request
CampusSchema.methods.findRequestsByVisitorStatus = async function findRequestsByVisitorStatus(
  { role, unit }, isDone, filters, offset, first, sort,
) {
  const Visitor = mongoose.model(VISITOR_MODEL_NAME);

  const stateValue = { 'workflow.steps': { $elemMatch: { 'role.template': role, 'state.value': { $exists: false } } } };
  const avoidRejected = {
    'workflow.steps': { $not: { $elemMatch: { behavior: WORKFLOW_BEHAVIOR_VALIDATION, 'state.isOK': false } } },
  };

  const notDoneFilter = {
    'request.units': unit
      ? { $elemMatch: { $and: [{ _id: mongoose.Types.ObjectId(unit) }, stateValue, avoidRejected] } }
      : { $elemMatch: { $and: [stateValue, avoidRejected] } },
    status: { $nin: [STATE_CANCELED] },
  };

  const doneFilter = {
    $or: [
      { visitors: { $not: { $elemMatch: notDoneFilter } } },
      { 'requestData.status': { $in: [STATE_CANCELED, STATE_REJECTED] } },
    ],
  };

  const nextStepsFilter = {
    $filter: {
      input: '$request.units.workflow.steps',
      as: 'step',
      cond: {
        $and: [
          { $ne: ['$$step.state.value', WORKFLOW_DECISION_ACCEPTED] },
          { $ne: ['$$step.state.value', WORKFLOW_DECISION_POSITIVE] },
          { $ne: ['$$step.state.value', WORKFLOW_DECISION_NEGATIVE] },
        ],
      },
    },
  };

  let visitorAggregate = Visitor.aggregate()
    .match(unit ? { 'request.units._id': mongoose.Types.ObjectId(unit) } : {});

  const execVisitorAggregate = async () => {
    const result = await visitorAggregate
      .addFields({ id: '$_id' })
      .project({ _id: 0, 'visitors._id': 0, 'requestData._id': 0 })
      .skip(offset)
      .limit(first)
      .sort(sort);
    return result;
  };

  visitorAggregate = isDone.value
    ? visitorAggregate
      .addFields({ id: { $toString: '$_id' } })
      .group({ _id: '$request._id', visitors: { $push: '$$ROOT' } })
      .lookup({
        from: 'requests', localField: '_id', foreignField: '_id', as: 'requestData',
      })
      .match({ ...filters, ...doneFilter })
    : visitorAggregate
      .match(notDoneFilter)
      .unwind('$request.units')
      .match(unit ? { 'request.units._id': mongoose.Types.ObjectId(unit) } : {})
      .addFields({ nextSteps: nextStepsFilter })
      .addFields({ nextStep: { $arrayElemAt: ['$nextSteps', 0] } })
      .match({ 'nextStep.role.template': role })
      .group({ _id: '$request._id', visitors: { $push: '$$ROOT' } })
      .lookup({
        from: 'requests', localField: '_id', foreignField: '_id', as: 'requestData',
      })
      .match({ 'requestData.status': STATE_CREATED });

  const countRequests = await visitorAggregate.exec();
  const requests = await execVisitorAggregate();

  return {
    list: requests,
    total: countRequests.length,
  };
};

CampusSchema.methods.uploadSecurityFile = async function uploadSecurityFile(campus, bucketName) {
  const ext = campus.file[0].files.file.filename
    .slice((campus.file[0].files.file.filename.lastIndexOf('.') - 1 >>> 0) + 2);
  const dbFilename = `doc${
    parseInt(campus.fileIndexChanged, 10) === 0 ? 'securityFile' : 'accesPlan'
  }_${campus.label.replace(/\s+/g, '')}_${campus.trigram}.${ext}`;
  return uploadFile(campus.file[0].files.file, dbFilename, bucketName);
};

export default mongoose.model(CAMPUS_MODEL_NAME, CampusSchema, 'campuses');
