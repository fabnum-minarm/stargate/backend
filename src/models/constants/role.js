import { WORKFLOW_BEHAVIOR_ADVISEMENT, WORKFLOW_BEHAVIOR_INFORMATION, WORKFLOW_BEHAVIOR_VALIDATION } from './unit';

export const VALIDATION_SCOPE_UNIT = 'VALIDATION_SCOPE_UNIT';
export const VALIDATION_SCOPE_CAMPUS = 'VALIDATION_SCOPE_CAMPUS';
export const VALIDATION_SCOPE_AREA = 'VALIDATION_SCOPE_AREA';
export const VALIDATION_SCOPE_ENUM = [
  VALIDATION_SCOPE_CAMPUS,
  VALIDATION_SCOPE_UNIT,
  VALIDATION_SCOPE_AREA,
];

export const HANDLE_CAMPUSES = 'HANDLE_CAMPUSES';
export const HANDLE_CAMPUS = 'HANDLE_CAMPUS';
export const HANDLE_USERS = 'HANDLE_USERS';
export const HANDLE_REQUEST = 'HANDLE_REQUEST';
export const HANDLE_VISITOR = 'HANDLE_VISITOR';
export const EXPORT_REQUEST = 'EXPORT_REQUEST';
export const PERMISSIONS_ENUM = [
  HANDLE_CAMPUSES,
  HANDLE_CAMPUS,
  HANDLE_USERS,
  HANDLE_REQUEST,
  HANDLE_VISITOR,
  EXPORT_REQUEST,
];

// Roles template
export const ROLE_SUPER_ADMIN = 'ROLE_SUPER_ADMIN';
export const ROLE_ADMIN = 'ROLE_ADMIN';
export const ROLE_BASIC_UNIT_VALIDATION = 'ROLE_BASIC_UNIT_VALIDATION';
export const ROLE_BASIC_CAMPUS_VALIDATION = 'ROLE_BASIC_CAMPUS_VALIDATION';
export const ROLE_FINAL_VALIDATION = 'ROLE_FINAL_VALIDATION';
export const ROLE_FINAL_VALIDATION_CFAS = 'ROLE_FINAL_VALIDATION_CFAS';
export const ROLE_FINAL_VALIDATION_OSB = 'ROLE_FINAL_VALIDATION_OSB';
export const ROLE_AREA_ADVISEMENT = 'ROLE_AREA_ADVISEMENT';
export const ROLE_UNIT_VALIDATION_TAGS = 'ROLE_UNIT_VALIDATION_TAGS';
export const ROLE_CAMPUS_INFORMATION = 'ROLE_CAMPUS_INFORMATION';
export const ROLE_HOST = 'ROLE_HOST';
export const ROLE_BASIC_USER = 'ROLE_BASIC_USER';
export const ROLE_SUPERADMIN_TEMPLATE = {
  template: ROLE_SUPER_ADMIN,
  label: 'Super Admin',
  shortLabel: 'SA',
  permissions: [HANDLE_CAMPUSES],
};
export const ROLE_ADMIN_TEMPLATE = {
  template: ROLE_ADMIN,
  permissions: [HANDLE_CAMPUS],
};
export const ROLE_BASIC_UNIT_VALIDATION_TEMPLATE = {
  template: ROLE_BASIC_UNIT_VALIDATION,
  validator: {
    scope: VALIDATION_SCOPE_UNIT,
    behavior: WORKFLOW_BEHAVIOR_VALIDATION,
  },
  permissions: [HANDLE_USERS, HANDLE_REQUEST, HANDLE_VISITOR],
};
export const ROLE_BASIC_CAMPUS_VALIDATION_TEMPLATE = {
  template: ROLE_BASIC_CAMPUS_VALIDATION,
  validator: {
    scope: VALIDATION_SCOPE_CAMPUS,
    behavior: WORKFLOW_BEHAVIOR_VALIDATION,
  },
  permissions: [HANDLE_USERS, HANDLE_REQUEST, HANDLE_VISITOR],
};
export const ROLE_FINAL_VALIDATION_TEMPLATE = {
  template: ROLE_FINAL_VALIDATION,
  validator: {
    scope: VALIDATION_SCOPE_CAMPUS,
    behavior: WORKFLOW_BEHAVIOR_VALIDATION,
  },
  permissions: [HANDLE_VISITOR, EXPORT_REQUEST],
};
export const ROLE_FINAL_VALIDATION_CFAS_TEMPLATE = {
  template: ROLE_FINAL_VALIDATION_CFAS,
  validator: {
    scope: VALIDATION_SCOPE_CAMPUS,
    behavior: WORKFLOW_BEHAVIOR_VALIDATION,
  },
  permissions: [HANDLE_VISITOR],
};
export const ROLE_FINAL_VALIDATION_OSB_TEMPLATE = {
  template: ROLE_FINAL_VALIDATION_OSB,
  validator: {
    scope: VALIDATION_SCOPE_CAMPUS,
    behavior: WORKFLOW_BEHAVIOR_VALIDATION,
  },
  permissions: [HANDLE_VISITOR],
};
export const ROLE_AREA_ADVISEMENT_TEMPLATE = {
  template: ROLE_AREA_ADVISEMENT,
  validator: {
    scope: VALIDATION_SCOPE_AREA,
    behavior: WORKFLOW_BEHAVIOR_ADVISEMENT,
  },
  permissions: [HANDLE_VISITOR],
};
export const ROLE_UNIT_VALIDATION_TAGS_TEMPLATE = {
  template: ROLE_UNIT_VALIDATION_TAGS,
  validator: {
    scope: VALIDATION_SCOPE_UNIT,
    behavior: WORKFLOW_BEHAVIOR_VALIDATION,
  },
  permissions: [HANDLE_REQUEST, HANDLE_VISITOR],
};
export const ROLE_CAMPUS_INFORMATION_TEMPLATE = {
  template: ROLE_CAMPUS_INFORMATION,
  validator: {
    scope: VALIDATION_SCOPE_CAMPUS,
    behavior: WORKFLOW_BEHAVIOR_INFORMATION,
  },
  permissions: [HANDLE_VISITOR],
};
export const ROLE_HOST_TEMPLATE = {
  template: ROLE_HOST,
  permissions: [HANDLE_REQUEST, HANDLE_VISITOR],
};
export const ROLE_BASIC_USER_TEMPLATE = {
  template: ROLE_BASIC_USER,
};
export const TEMPLATES_ROLES = [
  ROLE_SUPERADMIN_TEMPLATE,
  ROLE_ADMIN_TEMPLATE,
  ROLE_BASIC_UNIT_VALIDATION_TEMPLATE,
  ROLE_BASIC_CAMPUS_VALIDATION_TEMPLATE,
  ROLE_FINAL_VALIDATION_TEMPLATE,
  ROLE_FINAL_VALIDATION_CFAS_TEMPLATE,
  ROLE_FINAL_VALIDATION_OSB_TEMPLATE,
  ROLE_AREA_ADVISEMENT_TEMPLATE,
  ROLE_UNIT_VALIDATION_TAGS_TEMPLATE,
  ROLE_CAMPUS_INFORMATION_TEMPLATE,
  ROLE_HOST_TEMPLATE,
  ROLE_BASIC_USER_TEMPLATE,
];

// Basics roles for a campus creation
export const ROLE_ADMIN_INIT = {
  ...ROLE_ADMIN_TEMPLATE,
  label: 'Administrateur',
  shortLabel: 'Admin',
};

export const ROLE_UNIT_CORRESPONDENT_INIT = {
  ...ROLE_BASIC_UNIT_VALIDATION_TEMPLATE,
  label: 'Correspondant d\'unité',
  shortLabel: 'CU',
};

export const ROLE_SCREENING_INIT = {
  ...ROLE_AREA_ADVISEMENT_TEMPLATE,
  label: 'Criblage',
  shortLabel: 'Crib.',
};

export const ROLE_SECURITY_OFFICER_INIT = {
  ...ROLE_UNIT_VALIDATION_TAGS_TEMPLATE,
  label: 'Officier de sécurité',
  shortLabel: 'OS',
  tags: [
    {
      label: 'Visiteur Accompagné',
      value: 'VA',
      primary: true,
    },
    {
      label: 'Visiteur Libre',
      value: 'VL',
      primary: true,
    },
    {
      label: 'Visiteur Important',
      value: 'VIP',
      primary: true,
    },
  ],
};

export const ROLE_ACCESS_OFFICE_INIT = {
  ...ROLE_FINAL_VALIDATION_TEMPLATE,
  label: 'Bureau des Accès',
  shortLabel: 'BA',
  tags: [
    {
      label: 'Visiteur Accompagné',
      value: 'VA',
      primary: true,
    },
    {
      label: 'Visiteur Libre',
      value: 'VL',
      primary: true,
    },
    {
      label: 'Visiteur Important',
      value: 'VIP',
      primary: true,
    },
    {
      label: 'Hors MINARM Stagiaire accompagné',
      value: 'HMSA',
    },
    {
      label: 'Carte CIMS Nominative',
      value: 'CIMS',
    },
  ],
};

export const ROLE_CFAS_INIT = {
  ...ROLE_FINAL_VALIDATION_CFAS_TEMPLATE,
  label: 'CFAS',
  shortLabel: 'CFAS',
};

export const ROLE_OSB_INIT = {
  ...ROLE_FINAL_VALIDATION_OSB_TEMPLATE,
  label: 'Officier de Sécurité Base',
  shortLabel: 'OSB',
};

export const ROLE_HOST_INIT = {
  ...ROLE_HOST_TEMPLATE,
  label: 'Hôte',
  shortLabel: 'Hôte',
};

export const ROLE_GATEKEEPER_INIT = {
  ...ROLE_BASIC_USER_TEMPLATE,
  label: 'Gardien',
  shortLabel: 'Gard.',
};

export const ROLE_OBSERVER_INIT = {
  ...ROLE_BASIC_USER_TEMPLATE,
  label: 'Observateur',
  shortLabel: 'Obs.',
};

export const INIT_CAMPUS_ROLES = [
  ROLE_ADMIN_INIT,
  ROLE_UNIT_CORRESPONDENT_INIT,
  ROLE_SCREENING_INIT,
  ROLE_SECURITY_OFFICER_INIT,
  ROLE_ACCESS_OFFICE_INIT,
  ROLE_CFAS_INIT,
  ROLE_OSB_INIT,
  ROLE_HOST_INIT,
  ROLE_GATEKEEPER_INIT,
  ROLE_OBSERVER_INIT,
];
