import { rule } from 'graphql-shield';
import {
  EXPORT_REQUEST, HANDLE_CAMPUS, HANDLE_CAMPUSES, HANDLE_REQUEST, HANDLE_USERS, HANDLE_VISITOR,
} from './constants/role';

export { allow } from 'graphql-shield';
export const ROLE_SUPERADMIN = 'ROLE_SUPERADMIN';
export const ROLE_ADMIN = 'ROLE_ADMIN';
export const ROLE_UNIT_CORRESPONDENT = 'ROLE_UNIT_CORRESPONDENT';
export const ROLE_SECURITY_OFFICER = 'ROLE_SECURITY_OFFICER';
export const ROLE_ACCESS_OFFICE = 'ROLE_ACCESS_OFFICE';
export const ROLE_SCREENING = 'ROLE_SCREENING';
export const ROLE_HOST = 'ROLE_HOST';
export const ROLE_OBSERVER = 'ROLE_OBSERVER';

export const isAuthenticated = rule()(async (parent, args, ctx) => !!ctx.user);

export const isSuperAdmin = rule()(
  async (parent, args, ctx) => !!ctx.user.roles.find(({ role }) => role.permissions.includes(HANDLE_CAMPUSES)),
);

export const isAdmin = rule()(
  // @todo: Check campus too
  async (parent, args, ctx) => !!ctx.user.roles.find(({ role }) => role.permissions.includes(HANDLE_CAMPUS)),
);

export const canManageUsers = rule()(
  async (parent, args, ctx) => !!ctx.user.roles.find(({ role }) => role.permissions.includes(HANDLE_USERS)),
);

export const canHandleRequest = rule()(
  async (parent, args, ctx) => !!ctx.user.roles.find(({ role }) => role.permissions.includes(HANDLE_REQUEST)),
);

export const canHandleVisitor = rule()(
  async (parent, args, ctx) => !!ctx.user.roles.find(({ role }) => role.permissions.includes(HANDLE_VISITOR)),
);

export const canExportRequest = rule()(
  async (parent, args, ctx) => !!ctx.user.roles.find(({ role }) => role.permissions.includes(EXPORT_REQUEST)),
);
