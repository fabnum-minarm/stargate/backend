import mongoose from 'mongoose';
import { ZONE_MODEL_NAME } from './constants/modelNames';

const { Schema } = mongoose;

const ZoneSchema = new Schema({
  label: { type: String, required: true },
  campus: {
    _id: String,
    label: String,
    timezone: String,
  },
}, { timestamps: true });

export default mongoose.model(ZONE_MODEL_NAME, ZoneSchema, 'zones');
