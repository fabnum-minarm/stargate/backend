import { shield, or } from 'graphql-shield';
import {
  isAuthenticated,
  isSuperAdmin,
  isAdmin,
  allow,
  canHandleRequest,
  canHandleVisitor,
  canManageUsers,
  canExportRequest,
} from './rules';

export default shield({
  Query: {
    listCampuses: isSuperAdmin,
    listCampusAreas: or(isAdmin, isSuperAdmin),
    getCampus: isAuthenticated,
    getUser: or(isAdmin, isSuperAdmin, canManageUsers),
    me: isAuthenticated,
    listUsers: or(canManageUsers, isAdmin, isSuperAdmin),
    findUser: or(isAdmin, isSuperAdmin, canManageUsers),
    '*': isSuperAdmin,
  },
  Mutation: {
    login: allow,
    resetPassword: allow,
    sendResetPassword: allow,
    jwtRefresh: isAuthenticated,
    openIDRequest: allow,
    openIDLogin: allow,
    createUser: or(isAdmin, isSuperAdmin, canManageUsers),
    editUser: or(isAdmin, isSuperAdmin, canManageUsers),
    addUserRole: or(isAdmin, isSuperAdmin, canManageUsers),
    deleteUserRole: or(isAdmin, isSuperAdmin, canManageUsers),
    deleteUser: or(isAdmin, isSuperAdmin, canManageUsers),
    mutateCampus: or(isAdmin, isSuperAdmin, canHandleRequest, canHandleVisitor),
    editCampus: or(isAdmin, isSuperAdmin),
    editMe: isAuthenticated,
    addCampusRole: or(isAdmin, isSuperAdmin),
    editCampusRole: or(isAdmin, isSuperAdmin),
    '*': isSuperAdmin,
  },
  CampusMutation: {
    createRequest: or(canHandleRequest, isAdmin),
    editRequest: or(canHandleRequest, isAdmin),
    deleteRequest: or(canHandleRequest, isAdmin),
    mutateRequest: or(canHandleRequest, canHandleVisitor, isAdmin),
    shiftRequest: or(canHandleRequest, isAdmin),
    createPlace: or(isAdmin, isSuperAdmin),
    editPlace: or(isAdmin, isSuperAdmin),
    deletePlace: or(isAdmin, isSuperAdmin),
    generateCSVExportLink: or(canExportRequest, isAdmin),
    createUnit: or(isAdmin, isSuperAdmin),
    editUnit: or(isAdmin, isSuperAdmin),
    deleteUnit: or(isAdmin, isSuperAdmin),
    '*': isSuperAdmin,
  },
  RequestMutation: {
    createVisitor: or(canHandleRequest, isAdmin),
    editVisitor: or(canHandleRequest, isAdmin),
    deleteVisitor: or(canHandleRequest, isAdmin),
    cancelVisitor: or(canHandleRequest, canHandleVisitor, isAdmin),
  },
}, {
  allowExternalErrors: true,
});
