import mongoose from 'mongoose';
import { PLACE_MODEL_NAME, UNIT_MODEL_NAME } from './constants/modelNames';

const { Schema } = mongoose;

const PlaceSchema = new Schema({
  label: { type: String, required: true },
  campus: {
    _id: String,
    label: String,
    timezone: String,
  },
  zone: {
    _id: { type: Schema.ObjectId },
    label: { type: String },
  },
  unitInCharge: {
    _id: { type: Schema.ObjectId, alias: 'unitInCharge.id' },
    label: { type: String },
  },
}, { timestamps: true });

PlaceSchema.methods.setFromGraphQLSchema = async function setFromGraphQLSchema(data) {
  const Unit = mongoose.model(UNIT_MODEL_NAME);
  const filteredData = data;
  if (data.unitInCharge) {
    filteredData.unitInCharge = await Unit.findById(data.unitInCharge.id);
  }
  if (data.unitInCharge === null) {
    this.set('unitInCharge', null, { strict: false });
  }
  this.set(filteredData);
};

export default mongoose.model(PLACE_MODEL_NAME, PlaceSchema, 'places');
