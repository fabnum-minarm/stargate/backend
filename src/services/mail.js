import Nodemailer from 'nodemailer';
import Handlebars from 'handlebars';
import nodeFs from 'fs';
import nodePath from 'path';
import glob from 'glob';
import pino from 'pino';
import config from './config';
import Campus, { BUCKETNAME_CAMPUS_FILE } from "../models/campus";
import mongoose from "mongoose";

const logger = pino();
const { readFileSync } = nodeFs;
const { resolve, join } = nodePath;
const currentPath = __dirname;
const DEFAULT_LANG = 'fr';
const _ = require('lodash');

export default async function sendMail(recipients, options = {}) {
  const to = [].concat(recipients).join(';');
  const conf = config.get('mail:transporter');

  // snake case to camelcase for tls
  conf.tls = _.mapKeys(conf.tls, (value, key) => _.camelCase(key));

  if (!conf.auth || (!conf.auth.user && !conf.auth.pass)) {
    delete conf.auth;
  }
  const opts = {
    from: config.get('mail:default_from'),
    subject: '',
    text: '',
    ...options,
  };
  const transporter = Nodemailer.createTransport(conf);
  const mailOptions = { to, ...opts };
  try {
    return await transporter.sendMail(mailOptions);
  } catch (e) {
    logger.error(e.message);
    return Promise.resolve();
  }
}

function compileTemplates(path, ext) {
  return glob.sync(`${path}.*${ext}`)
    .reduce((acc, curr) => {
      // ext variable come from code
      // eslint-disable-next-line security/detect-non-literal-regexp
      const langRegex = new RegExp(`([a-z]+)${ext.replace('.', '\\.')}$`);
      const [, lang] = langRegex.exec(curr);
      return Object.assign(acc, {
        [lang]: Handlebars.compile(readFileSync(curr).toString()),
      });
    }, {});
}

const getCampusFileStream = async (fileId) => {
  const bucket = new mongoose.mongo.GridFSBucket(mongoose.connection.db, { bucketName: BUCKETNAME_CAMPUS_FILE });
  const id = mongoose.Types.ObjectId(fileId);
  return bucket.openDownloadStream(id);
};

export function prepareSendMailFromTemplate(template, subject, attachments = []) {
  const path = resolve(join(currentPath, '..', 'templates', 'mail', template));
  const templates = {
    txt: compileTemplates(path, '.txt.hbs'),
  };

  return async (to, { data = {}, lang = DEFAULT_LANG }) => {
    // Input come from code
    // eslint-disable-next-line security/detect-object-injection
    if (!templates.txt[lang]) {
      throw new Error('No mail template found');
    }

    const opts = {
      subject,
      attachments
    };
    // Input come from code
    // eslint-disable-next-line security/detect-object-injection
    if (templates.txt[lang]) {
      // Input come from code
      // eslint-disable-next-line security/detect-object-injection
      const ct = templates.txt[lang](data);
      opts.text = ct;
    }
    // @todo: async in queue management
    sendMail(to, opts);
  };
}

export const sendPasswordResetMail = prepareSendMailFromTemplate(
  'password-reset',
  'Réinitialisation de votre mot de passe Stargate',
);

export const sendUserCreationMail = prepareSendMailFromTemplate(
  'create-user',
  'Initialisation de votre mot de passe Stargate',
);

export const sendRequestCreationMail = (base, from) => prepareSendMailFromTemplate(
  'request-creation',
  `Demande d'accès ${base} le ${from}`,
);

export const sendRequestValidationStepMail = (from) => prepareSendMailFromTemplate(
  'request-validation-step',
  `Validation demande d'accès pour le ${from}`,
);

export const sendRequestValidatedOwnerMail = (base, from) => prepareSendMailFromTemplate(
  'request-validated-owner',
  `Votre demande d'accès pour ${base} le ${from}`,
);

export const sendSecurityNotificationEmail = async (owner, from, visitor, attachments) => prepareSendMailFromTemplate(
  'request-security-notification',
  `Notification demande d'accès pour ${visitor.birthLastname} ${visitor.firstname} (${visitor.nationality}) par ${owner} pour le ${from}`,
  attachments,
);

export const sendRequestAcceptedVisitorMail = async (base, from, campus) => {
  const c = await Campus.findById(campus._id);
  const attachments = [];
  for (const cd of c.campusDocuments) {
    attachments.push({
      filename: cd.file.filename,
      content: await getCampusFileStream(cd.file._id)
    });
  }
  return prepareSendMailFromTemplate(
      'request-accepted-visitor',
      `Votre accès ${base} le ${from}`,
      attachments,
  );
};

export const sendRequestRefusedVisitorMail = async (base, from) => prepareSendMailFromTemplate(
  'request-refused-visitor',
  `Votre accès ${base} le ${from}`,
);
