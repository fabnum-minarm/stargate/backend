import mongoose from 'mongoose';
import {
  NAVAL_BASE,
} from './campus';
import {
  UNIT_CIRI,
  UNIT_FUS,
  UNIT_SAILOR,
  UNIT_BBPD,
  UNIT_LASEM,
} from './unit';
import {
  ROLE_ACCESS_OFFICE_INIT,
  ROLE_ADMIN_INIT,
  ROLE_CFAS_INIT,
  ROLE_OSB_INIT,
  ROLE_GATEKEEPER_INIT,
  ROLE_HOST_INIT,
  ROLE_SCREENING_INIT,
  ROLE_SECURITY_OFFICER_INIT,
  ROLE_SUPERADMIN_TEMPLATE,
  ROLE_UNIT_CORRESPONDENT_INIT,
} from '../../src/models/constants/role';

export const SAMId = new mongoose.Types.ObjectId();

export default async ({ log }) => {
  const users = [
    {
      firstname: 'Super',
      lastname: 'Admin',
      email: {
        original: 'super.admin@localhost',
      },
      password: 'testtest',
      roles: [
        { role: ROLE_SUPERADMIN_TEMPLATE },
        { role: ROLE_ADMIN_INIT, campuses: [NAVAL_BASE] },
      ],
    },
    {
      firstname: 'BA',
      lastname: 'BaseNavale',
      email: {
        original: 'ba.basenavale@localhost',
      },
      password: 'testtest',
      roles: [
        { role: ROLE_ACCESS_OFFICE_INIT, campuses: [NAVAL_BASE] },
      ],
    },
    {
      firstname: 'CFAS',
      lastname: 'BaseNavale',
      email: {
        original: 'cfas.basenavale@localhost',
      },
      password: 'testtest',
      roles: [
        { role: ROLE_CFAS_INIT, campuses: [NAVAL_BASE] },
      ],
    },
    {
      firstname: 'Officier de Sécurité',
      lastname: 'BaseNavale',
      email: {
        original: 'osb.basenavale@localhost',
      },
      password: 'testtest',
      roles: [
        { role: ROLE_OSB_INIT, campuses: [NAVAL_BASE] },
      ],
    },
    {
      firstname: 'Gendarme',
      lastname: 'BaseNavale',
      email: {
        original: 'gmd.basenavale@localhost',
      },
      password: 'testtest',
      roles: [
        { role: ROLE_SCREENING_INIT, campuses: [NAVAL_BASE] },
      ],
    },
    {
      firstname: 'Gardien',
      lastname: 'BaseNavale',
      email: {
        original: 'grd.basenavale@localhost',
      },
      password: 'testtest',
      roles: [
        { role: ROLE_GATEKEEPER_INIT, campuses: [NAVAL_BASE] },
      ],
    },
    {
      firstname: 'Demandeur',
      lastname: 'CIRI',
      email: {
        original: 'demandeur.ciri@localhost',
      },
      password: 'testtest',
      roles: [
        { role: ROLE_HOST_INIT, units: [UNIT_CIRI], campuses: [NAVAL_BASE] },
      ],
    },
    {
      firstname: 'CU',
      lastname: 'CIRI',
      email: {
        original: 'cu.ciri@localhost',
      },
      password: 'testtest',
      roles: [
        { role: ROLE_UNIT_CORRESPONDENT_INIT, units: [UNIT_CIRI], campuses: [NAVAL_BASE] },
      ],
    },
    {
      firstname: 'OS',
      lastname: 'CIRI',
      email: {
        original: 'os.ciri@localhost',
      },
      password: 'testtest',
      roles: [
        { role: ROLE_SECURITY_OFFICER_INIT, units: [UNIT_CIRI], campuses: [NAVAL_BASE] },
      ],
    },
    {
      firstname: 'Demandeur',
      lastname: 'FUS',
      email: {
        original: 'demandeur.fus@localhost',
      },
      password: 'testtest',
      roles: [
        { role: ROLE_HOST_INIT, units: [UNIT_FUS], campuses: [NAVAL_BASE] },
      ],
    },
    {
      firstname: 'CU',
      lastname: 'FUS',
      email: {
        original: 'cu.fus@localhost',
      },
      password: 'testtest',
      roles: [
        { role: ROLE_UNIT_CORRESPONDENT_INIT, units: [UNIT_FUS], campuses: [NAVAL_BASE] },
      ],
    },
    {
      firstname: 'OS',
      lastname: 'FUS',
      email: {
        original: 'os.fus@localhost',
      },
      password: 'testtest',
      roles: [
        { role: ROLE_SECURITY_OFFICER_INIT, units: [UNIT_FUS], campuses: [NAVAL_BASE] },
      ],
    },
    {
      firstname: 'Eddy',
      lastname: 'Moitout',
      email: {
        original: 'eddy.moitout@localhost',
      },
      password: 'testtest',
      roles: [
        { role: ROLE_SCREENING_INIT, campuses: [NAVAL_BASE] },
      ],
    },
    {
      firstname: 'Michaël',
      lastname: 'Mcaradech',
      email: {
        original: 'michael.mcaradech@localhost',
      },
      password: 'testtest',
      roles: [
        { role: ROLE_ACCESS_OFFICE_INIT, campuses: [NAVAL_BASE] },
      ],
    },
    {
      firstname: 'Oscar',
      lastname: 'Amel',
      email: {
        original: 'oscar.amel@localhost',
      },
      password: 'testtest',
      roles: [
        { role: ROLE_HOST_INIT, units: [UNIT_SAILOR], campuses: [NAVAL_BASE] },
      ],
    },
    {
      firstname: 'Denis',
      lastname: 'Doiseau',
      email: {
        original: 'denis.doiseau@localhost',
      },
      password: 'testtest',
      roles: [
        { role: ROLE_HOST_INIT, units: [UNIT_SAILOR], campuses: [NAVAL_BASE] },
      ],
    },
    {
      firstname: 'Anne',
      lastname: 'Onyme',
      email: {
        original: 'anne.onyme@localhost',
      },
      password: 'testtest',
      roles: [
        { role: ROLE_HOST_INIT, units: [UNIT_SAILOR], campuses: [NAVAL_BASE] },
      ],
    },
    {
      firstname: 'Alain',
      lastname: 'Terieur',
      email: {
        original: 'alain.terieur@localhost',
      },
      password: 'testtest',
      roles: [
        { role: ROLE_UNIT_CORRESPONDENT_INIT, units: [UNIT_SAILOR], campuses: [NAVAL_BASE] },
      ],
    },
    {
      firstname: 'Anna',
      lastname: 'Lyz',
      email: {
        original: 'anna.lyz@localhost',
      },
      password: 'testtest',
      roles: [
        { role: ROLE_UNIT_CORRESPONDENT_INIT, units: [UNIT_SAILOR], campuses: [NAVAL_BASE] },
      ],
    },
    {
      firstname: 'Pat',
      lastname: 'Atatrak',
      email: {
        original: 'pat.atatrak@localhost',
      },
      password: 'testtest',
      roles: [
        { role: ROLE_SECURITY_OFFICER_INIT, units: [UNIT_SAILOR], campuses: [NAVAL_BASE] },
      ],
    },
    {
      firstname: 'Alain',
      lastname: 'Die',
      email: {
        original: 'alain.die@localhost',
      },
      password: 'testtest',
      roles: [
        { role: ROLE_HOST_INIT, units: [UNIT_BBPD], campuses: [NAVAL_BASE] },
      ],
    },
    {
      _id: SAMId,
      firstname: 'Sam',
      lastname: 'Soule',
      email: {
        original: 'sam.soule@localhost',
      },
      password: 'testtest',
      roles: [
        { role: ROLE_UNIT_CORRESPONDENT_INIT, units: [UNIT_BBPD], campuses: [NAVAL_BASE] },
        { role: ROLE_SECURITY_OFFICER_INIT, units: [UNIT_BBPD], campuses: [NAVAL_BASE] },
      ],
    },
    {
      firstname: 'Jean',
      lastname: 'Tourloupe',
      email: {
        original: 'jean.tourloupe@localhost',
      },
      password: 'testtest',
      roles: [
        { role: ROLE_HOST_INIT, units: [UNIT_LASEM], campuses: [NAVAL_BASE] },
      ],
    },
    {
      firstname: 'Jean',
      lastname: 'Sérien',
      email: {
        original: 'jean.serien@localhost',
      },
      password: 'testtest',
      roles: [
        { role: ROLE_UNIT_CORRESPONDENT_INIT, units: [UNIT_LASEM], campuses: [NAVAL_BASE] },
      ],
    },
  ];

  users.forEach((user) => log.info(`${user.email.original} password will be ${user.password}`));

  return users;
};
