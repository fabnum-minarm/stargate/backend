import { NAVAL_BASE } from './campus';

export const AREA_ONE = {
  label: 'Zone Influence 1',
  campuses: [NAVAL_BASE],
};

export default async () => [
  AREA_ONE,
];
