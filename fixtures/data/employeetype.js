import mongoose from 'mongoose';

export const EMPLOYEE_TYPE_CIVDEF_ID = new mongoose.Types.ObjectId();
export const EMPLOYEE_TYPE_PMM_ID = new mongoose.Types.ObjectId();
export const EMPLOYEE_TYPE_LIVREUR_ID = new mongoose.Types.ObjectId();

export const EMPLOYEE_TYPE_CIVDEF = {
  _id: EMPLOYEE_TYPE_CIVDEF_ID,
  label: 'Civil de la défense',
  campus: {
    _id: 'NAVAL-BASE',
    label: 'Base Navale',
  },
};

export const EMPLOYEE_TYPE_PMM = {
  _id: EMPLOYEE_TYPE_PMM_ID,
  label: 'PMM',
  campus: {
    _id: 'NAVAL-BASE',
    label: 'Base Navale',
  },
};

export const EMPLOYEE_TYPE_LIVREUR = {
  _id: EMPLOYEE_TYPE_LIVREUR_ID,
  label: 'Livreur',
  campus: {
    _id: 'NAVAL-BASE',
    label: 'Base Navale',
  },
};

export default async () => [
  EMPLOYEE_TYPE_CIVDEF,
  EMPLOYEE_TYPE_PMM,
  EMPLOYEE_TYPE_LIVREUR,
];
