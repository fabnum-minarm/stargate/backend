import {
  ROLE_ACCESS_OFFICE_INIT,
  ROLE_ADMIN_INIT,
  ROLE_CFAS_INIT,
  ROLE_OSB_INIT,
  ROLE_GATEKEEPER_INIT,
  ROLE_HOST_INIT,
  ROLE_SCREENING_INIT,
  ROLE_SECURITY_OFFICER_INIT,
  ROLE_UNIT_CORRESPONDENT_INIT,
} from '../../src/models/constants/role';

export const NAVAL_BASE = {
  _id: 'NAVAL-BASE',
  label: 'Base Navale',
  trigram: 'NVL',
  securityNotificationEmail: 'security@localhost',
  securityNotificationEnabled: true,
  roles: [ROLE_UNIT_CORRESPONDENT_INIT,
    ROLE_SCREENING_INIT,
    ROLE_SECURITY_OFFICER_INIT,
    ROLE_ACCESS_OFFICE_INIT,
    ROLE_HOST_INIT,
    ROLE_GATEKEEPER_INIT,
    ROLE_ADMIN_INIT,
    ROLE_CFAS_INIT,
    ROLE_OSB_INIT
  ],
};

export default async () => [
  NAVAL_BASE,
];
