import mongoose from 'mongoose';
import {
  ROLE_ACCESS_OFFICE_INIT,
  ROLE_SCREENING_INIT,
  ROLE_SECURITY_OFFICER_INIT,
  ROLE_UNIT_CORRESPONDENT_INIT,
} from '../../src/models/constants/role';

export const ciriId = new mongoose.Types.ObjectId();
export const fusId = new mongoose.Types.ObjectId();
export const sailorId = new mongoose.Types.ObjectId();
export const bbpdId = new mongoose.Types.ObjectId();
export const lasemId = new mongoose.Types.ObjectId();

export const UNIT_CIRI = {
  _id: ciriId,
  label: 'CIRI',
  trigram: 'CIR',
  campus: {
    _id: 'NAVAL-BASE',
    label: 'Base Navale',
  },
  workflow: {
    steps: [
      {
        role: ROLE_UNIT_CORRESPONDENT_INIT,
      },
      {
        role: ROLE_SCREENING_INIT,
      },
      {
        role: ROLE_SECURITY_OFFICER_INIT,
      },
      {
        role: ROLE_ACCESS_OFFICE_INIT,
      },
    ],
  },
};

export const UNIT_FUS = {
  _id: fusId,
  label: 'FUS',
  trigram: 'FUS',
  campus: {
    _id: 'NAVAL-BASE',
    label: 'Base Navale',
  },
  workflow: {
    steps: [
      {
        role: ROLE_UNIT_CORRESPONDENT_INIT,
      },
      {
        role: ROLE_SCREENING_INIT,
      },
      {
        role: ROLE_SECURITY_OFFICER_INIT,
        condition: {
          role: ROLE_SCREENING_INIT.template,
          value: false,
        },
      },
      {
        role: ROLE_ACCESS_OFFICE_INIT,
      },
    ],
  },
};

export const UNIT_SAILOR = {
  _id: sailorId,
  label: 'Marins-Pompiers',
  trigram: 'MPO',
  campus: {
    _id: 'NAVAL-BASE',
    label: 'Base Navale',
  },
  workflow: {
    steps: [
      {
        role: ROLE_UNIT_CORRESPONDENT_INIT,
      },
      {
        role: ROLE_SCREENING_INIT,
      },
      {
        role: ROLE_SECURITY_OFFICER_INIT,
      },
      {
        role: ROLE_ACCESS_OFFICE_INIT,
      },
    ],
  },
};

export const UNIT_BBPD = {
  _id: bbpdId,
  label: 'BBPD Acanthe',
  trigram: 'BB',
  campus: {
    _id: 'NAVAL-BASE',
    label: 'Base Navale',
  },
  workflow: {
    steps: [
      {
        role: ROLE_UNIT_CORRESPONDENT_INIT,
      },
      {
        role: ROLE_SCREENING_INIT,
      },
      {
        role: ROLE_SECURITY_OFFICER_INIT,
      },
      {
        role: ROLE_ACCESS_OFFICE_INIT,
      },
    ],
  },
};

export const UNIT_LASEM = {
  _id: lasemId,
  label: 'Lasem',
  trigram: 'LSM',
  campus: {
    _id: 'NAVAL-BASE',
    label: 'Base Navale',
  },
  workflow: {
    steps: [
      {
        role: ROLE_UNIT_CORRESPONDENT_INIT,
      },
      {
        role: ROLE_SCREENING_INIT,
      },
      {
        role: ROLE_ACCESS_OFFICE_INIT,
      },
    ],
  },
};

export default async () => [
  UNIT_CIRI,
  UNIT_FUS,
  UNIT_SAILOR,
  UNIT_BBPD,
  UNIT_LASEM,
];
