import { nanoid } from 'nanoid';
import queryFactory, { gql } from '../../helpers/apollo-query';
import { generateDummySuperAdmin } from '../../models/user';
import Campus, { createDummyCampus } from '../../models/campus';
import {
  ROLE_BASIC_CAMPUS_VALIDATION_TEMPLATE,
  ROLE_BASIC_UNIT_VALIDATION,
  ROLE_BASIC_UNIT_VALIDATION_TEMPLATE,
} from '../../../src/models/constants/role';
import generateRoleFromTemplate from '../../helpers/generateRoleFromTemplate';

function mutateAddCampusCampus(id, role, user = null) {
  const { mutate } = queryFactory(user);
  return mutate({
    mutation: gql`
            mutation AddCampusRoleMutation($id: String!, $role: RoleInput!) {
                addCampusRole(id: $id, role: $role) {
                    id
                    roles {
                        label
                        permissions
                    }
                }
            }
        `,
    variables: { id, role },
  });
}

it('Test to add a role to a campus', async () => {
  const dummyCampus = await createDummyCampus({
    roles: [generateRoleFromTemplate(ROLE_BASIC_CAMPUS_VALIDATION_TEMPLATE)],
  });
  const newRoleData = {
    template: ROLE_BASIC_UNIT_VALIDATION,
    label: nanoid(),
    shortLabel: nanoid(3),
  };
  try {
    {
      const { errors } = await mutateAddCampusCampus(dummyCampus._id, newRoleData);

      // You're not authorized to edit campus roles while without rights
      expect(errors).toHaveLength(1);
      expect(errors[0].message).toContain('Not Authorised');
    }

    {
      const { errors } = await mutateAddCampusCampus(nanoid(), newRoleData, generateDummySuperAdmin());

      expect(errors).toHaveLength(1);
      expect(errors[0].message).toContain('Campus not found');
    }

    {
      const { data: { addCampusRole } } = await mutateAddCampusCampus(
        dummyCampus._id,
        newRoleData,
        generateDummySuperAdmin(),
      );
      expect(addCampusRole.roles).toHaveLength(2);
      expect(addCampusRole.roles[1]).toHaveProperty('label', newRoleData.label);
      expect(addCampusRole.roles[1]).toHaveProperty('permissions', ROLE_BASIC_UNIT_VALIDATION_TEMPLATE.permissions);
      const dbVersion = await Campus.findOne({ _id: dummyCampus._id });
      expect(dbVersion.roles).toHaveLength(2);
      expect(dbVersion).toHaveProperty('__v', 1);
    }
  } finally {
    await Campus.findOneAndDelete({ _id: dummyCampus._id });
  }
});
