import { nanoid } from 'nanoid';
import queryFactory, { gql } from '../../helpers/apollo-query';
import User, { createDummyUser, generateDummySuperAdmin } from '../../models/user';
import Campus, { createDummyCampus, generateDummyCampus } from '../../models/campus';
import {
  ROLE_BASIC_CAMPUS_VALIDATION,
  ROLE_BASIC_CAMPUS_VALIDATION_TEMPLATE,
  ROLE_BASIC_USER_TEMPLATE,
} from '../../../src/models/constants/role';
import generateRoleFromTemplate from '../../helpers/generateRoleFromTemplate';
import Unit, { createDummyUnit } from '../../models/unit';

function mutateEditCampusCampus(id, shortLabel, role, user = null) {
  const { mutate } = queryFactory(user);
  return mutate({
    mutation: gql`
            mutation EditCampusRoleMutation($id: String!, $shortLabel: String!, $role: RoleInput!) {
                editCampusRole(id: $id, shortLabel: $shortLabel, role: $role) {
                    id
                    roles {
                    template
                        label
                        permissions
                        shortLabel
                    }
                }
            }
        `,
    variables: { id, shortLabel, role },
  });
}
jest.setTimeout(30000);
it('Test to add a role to a campus', async () => {
  const role = generateRoleFromTemplate(ROLE_BASIC_CAMPUS_VALIDATION_TEMPLATE);
  const dummyCampus = await createDummyCampus({ roles: [role] });
  const dummyUnit = await createDummyUnit({
    campus: generateDummyCampus(),
    workflow: {
      steps: [
        { role },
      ],
    },
  });
  const dummyUnitToUpdate = await createDummyUnit({
    campus: dummyCampus,
    workflow: {
      steps: [
        { role },
        { role: generateRoleFromTemplate(ROLE_BASIC_USER_TEMPLATE) },
      ],
    },
  });
  const dummyUser = await createDummyUser({
    roles: [{ role, campuses: [dummyCampus] }, { role, campuses: [generateDummyCampus()] }],
  });

  try {
    const newRoleData = {
      template: ROLE_BASIC_CAMPUS_VALIDATION,
      label: nanoid(),
      shortLabel: nanoid(3),
    };
    {
      const { errors } = await mutateEditCampusCampus(dummyCampus._id, dummyCampus.roles[0].shortLabel, newRoleData);

      // You're not authorized to edit role campus roles while without rights
      expect(errors).toHaveLength(1);
      expect(errors[0].message).toContain('Not Authorised');
    }

    {
      const { errors } = await mutateEditCampusCampus(
        nanoid(), dummyCampus.roles[0].shortLabel, newRoleData, generateDummySuperAdmin(),
      );

      expect(errors).toHaveLength(1);
      expect(errors[0].message).toContain('Campus not found');
    }

    {
      const { data: { editCampusRole } } = await mutateEditCampusCampus(
        dummyCampus._id,
        dummyCampus.roles[0].shortLabel,
        newRoleData,
        generateDummySuperAdmin(),
      );

      expect(editCampusRole.roles[0]).toHaveProperty('label', newRoleData.label);
      const unitToUpdateDbVersion = await Unit.findById(dummyUnitToUpdate._id);
      expect(unitToUpdateDbVersion.workflow.steps[0].role).toHaveProperty('label', newRoleData.label);
      const unitDbVersion = await Unit.findById(dummyUnit._id);
      expect(unitDbVersion.workflow.steps[0].role).toHaveProperty('label', role.label);
      const campusDbVersion = await Campus.findOne({ _id: dummyCampus._id });
      const userDbVersion = await User.findById(dummyUser._id);
      expect(userDbVersion.roles[0].role).toHaveProperty('label', newRoleData.label);
      expect(userDbVersion.roles[1].role).toHaveProperty('label', role.label);
      expect(campusDbVersion).toHaveProperty('__v', 1);
    }
  } finally {
    await Campus.findOneAndDelete({ _id: dummyCampus._id });
    await User.findOneAndDelete({ _id: dummyUser._id });
    await Unit.deleteMany();
  }
});
