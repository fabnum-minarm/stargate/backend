import mongoose from 'mongoose';
import queryFactory, { gql } from '../../helpers/apollo-query';
import { generateDummyAdmin, generateDummyUser } from '../../models/user';
import { createDummyRequest } from '../../models/request';
import { createDummyCampus } from '../../models/campus';
import Visitor, { createDummyVisitor } from '../../models/visitor';
import { createDummyUnit } from '../../models/unit';
import {
  WORKFLOW_BEHAVIOR_ACK,
  WORKFLOW_DECISION_ACCEPTED,
  WORKFLOW_DECISION_POSITIVE,
  WORKFLOW_DECISION_REJECTED,
} from '../../../src/models/constants/unit';
import Place, { generateDummyPlace } from '../../models/place';
import { EVENT_CREATE } from '../../../src/models/request';
import {
  ROLE_AREA_ADVISEMENT_TEMPLATE,
  ROLE_BASIC_CAMPUS_VALIDATION,
  ROLE_BASIC_CAMPUS_VALIDATION_TEMPLATE,
  ROLE_BASIC_UNIT_VALIDATION_TEMPLATE,
  ROLE_CAMPUS_INFORMATION_TEMPLATE,
} from '../../../src/models/constants/role';

function mutatevalidateStepRequest(campusId, requestId, visitorId, personas, decision, tags = [], user = null) {
  const { mutate } = queryFactory(user);
  return mutate({
    mutation: gql`
      mutation validateStepRequestMutation(
        $campusId: String!,
        $requestId: String!,
        $visitorId: ObjectID!,
        $personas: ValidationPersonas!,
        $decision: String!
        $tags: [String]
      ) {
        mutateCampus(id: $campusId) {
          mutateRequest(id: $requestId) {
            validateVisitorStep(id: $visitorId, as: $personas, decision: $decision, tags: $tags) {
              id
              firstname
              status
              units {
                steps {
                  role {
                      label
                      validator {
                          behavior
                      }
                  }
                  state {
                    value
                    isOK
                    date
                    tags
                  }
                }
              }
            }
          }
        }
      }
    `,
    variables: {
      campusId,
      requestId: requestId.toString ? requestId.toString() : requestId,
      visitorId: visitorId.toString ? visitorId.toString() : visitorId,
      personas,
      decision,
      tags,
    },
  });
}

it('Test to validate a step for a visitor', async () => {
  const campus = await createDummyCampus();
  const roleCampusValidation = ROLE_BASIC_CAMPUS_VALIDATION_TEMPLATE;
  const roleScreening = ROLE_AREA_ADVISEMENT_TEMPLATE;
  const roleObserver = ROLE_CAMPUS_INFORMATION_TEMPLATE;
  const roleUnitCorrespondent = ROLE_BASIC_UNIT_VALIDATION_TEMPLATE;
  const unit1 = await createDummyUnit({
    campus,
    workflow: {
      steps: [
        {
          role: roleCampusValidation,
        },
        {
          role: roleScreening,
        },
        {
          role: roleObserver,
        },
        {
          role: roleUnitCorrespondent,
        },
      ],
    },
  });
  const unit2 = await createDummyUnit({
    campus,
    workflow: {
      steps: [
        {
          role: roleUnitCorrespondent,
        },
        {
          role: roleScreening,
        },
      ],
    },
  });

  const place1 = new Place(generateDummyPlace({ campus, unitInCharge: unit1 }));
  const place2 = new Place(generateDummyPlace({ campus, unitInCharge: unit1 }));
  const place3 = new Place(generateDummyPlace({ campus, unitInCharge: unit2 }));
  const owner = await generateDummyUser({ unit: unit1 });

  const request = await createDummyRequest({
    campus,
    owner,
    places: [
      place1,
      place2,
      place3,
    ],
  });

  const visitor = await createDummyVisitor({
    request,
    firstname: 'Foo',
    birthLastname: 'Bar',
    usageLastname: 'Bar',
    birthday: new Date('1970-01-01'),
    birthdayPlace: 'Paris',
  });
  request.stateMutation(EVENT_CREATE);
  await request.save();

  try {
    {
      const { errors } = await mutatevalidateStepRequest(
        campus._id,
        request._id,
        visitor._id,
        {},
        '',
      );

      // You're not authorized to create request while without rights
      expect(errors).toHaveLength(1);
      expect(errors[0].message).toContain('Not Authorised');
    }
    {
      const { errors } = await mutatevalidateStepRequest(
        campus._id,
        request._id,
        new mongoose.Types.ObjectId(),
        {},
        '',
        [],
        generateDummyAdmin(),
      );
      // You're should not mutate a visitor that not exists.
      expect(errors).toHaveLength(1);
      expect(errors[0].message).toContain('Visitor not found');
    }
    {
      const { errors } = await mutatevalidateStepRequest(
        campus._id,
        request._id,
        visitor._id,
        {
          unit: unit1._id.toString(),
          role: roleCampusValidation.template,
        },
        'foo',
        [],
        generateDummyAdmin(),
      );
      expect(errors).toHaveLength(1);
      expect(errors[0].message).toContain('Validation behavior cannot accept "foo" decision.');
    }
    {
      const { errors } = await mutatevalidateStepRequest(
        campus._id,
        request._id,
        visitor._id,
        {
          unit: unit1._id.toString(),
          role: roleObserver.template,
        },
        WORKFLOW_BEHAVIOR_ACK,
        [],
        generateDummyAdmin(),
      );
      expect(errors).toHaveLength(1);
      expect(errors[0].message).toContain('Previous step for role ROLE_CAMPUS_INFORMATION not yet validated');
    }
    {
      const { errors } = await mutatevalidateStepRequest(
        campus._id,
        request._id,
        visitor._id,
        {
          role: roleScreening.template,
        },
        WORKFLOW_DECISION_POSITIVE,
        [],
        generateDummyAdmin(),
      );
      expect(errors).toHaveLength(1);
      expect(errors[0].message).toContain('Previous step for role ROLE_AREA_ADVISEMENT not yet validated');
    }
    {
      const TAG = 'TAG';
      const { data: { mutateCampus: { mutateRequest: { validateVisitorStep } } } } = await mutatevalidateStepRequest(
        campus._id,
        request._id,
        visitor._id,
        {
          unit: unit1._id.toString(),
          role: roleCampusValidation.template,
        },
        WORKFLOW_DECISION_ACCEPTED,
        [TAG],
        generateDummyAdmin(),
      );
      const dbVersion = await Visitor.findById(validateVisitorStep.id);
      const dbUnit = dbVersion.request.units.find((u) => u._id.equals(unit1._id));
      const dbStep = dbUnit.workflow.steps.find((s) => s._id.equals(unit1.workflow.steps[0]._id));
      expect(dbStep.state.value).toEqual(WORKFLOW_DECISION_ACCEPTED);
      expect(dbStep.state.payload.tags).toEqual(expect.arrayContaining([TAG]));
      expect(dbVersion).toHaveProperty('__v', 1);
    }
    {
      await mutatevalidateStepRequest(
        campus._id,
        request._id,
        visitor._id,
        {
          unit: unit2._id.toString(),
          role: roleUnitCorrespondent.template,
        },
        WORKFLOW_DECISION_REJECTED,
        [],
        generateDummyAdmin(),
      );
      const { data: { mutateCampus: { mutateRequest: { validateVisitorStep } } } } = await mutatevalidateStepRequest(
        campus._id,
        request._id,
        visitor._id,
        {
          role: roleScreening.template,
        },
        WORKFLOW_DECISION_POSITIVE,
        [],
        generateDummyAdmin(),
      );
      const dbVersion = await Visitor.findById(validateVisitorStep.id);
      const dbUnit = dbVersion.request.units.find((u) => u._id.equals(unit1._id));
      const dbStep = dbUnit.workflow.steps.find((s) => s._id.equals(unit1.workflow.steps[1]._id));
      expect(dbStep.state.value).toEqual(WORKFLOW_DECISION_POSITIVE);
      expect(dbVersion).toHaveProperty('__v', 3);
    }
    {
      const { data: { mutateCampus: { mutateRequest: { validateVisitorStep } } } } = await mutatevalidateStepRequest(
        campus._id,
        request._id,
        visitor._id,
        {
          unit: unit1._id.toString(),
          role: roleObserver.template,
        },
        WORKFLOW_BEHAVIOR_ACK,
        [],
        generateDummyAdmin(),
      );
      const dbVersion = await Visitor.findById(validateVisitorStep.id);
      const dbUnit = dbVersion.request.units.find((u) => u._id.equals(unit1._id));
      const dbStep = dbUnit.workflow.steps.find((s) => s._id.equals(unit1.workflow.steps[2]._id));
      expect(dbStep.state.value).toEqual(WORKFLOW_BEHAVIOR_ACK);
      expect(dbVersion).toHaveProperty('__v', 4);
    }
  } finally {
    await visitor.deleteOne();
    await request.deleteOne();
    await campus.deleteOne();
  }
});

it('Test to validate a step for a visitor with role condition', async () => {
  const campus = await createDummyCampus();
  const roleCampusValidation = ROLE_BASIC_CAMPUS_VALIDATION_TEMPLATE;
  const roleScreening = ROLE_AREA_ADVISEMENT_TEMPLATE;
  const roleObserver = ROLE_CAMPUS_INFORMATION_TEMPLATE;
  const unit1 = await createDummyUnit({
    campus,
    workflow: {
      steps: [
        {
          role: roleCampusValidation,
        },
        {
          role: roleScreening,
          condition: { role: ROLE_BASIC_CAMPUS_VALIDATION, value: true },
        },
        {
          role: roleObserver,
        },
      ],
    },
  });

  const place1 = new Place(generateDummyPlace({ campus, unitInCharge: unit1 }));
  const owner = await generateDummyUser({ unit: unit1 });

  const request = await createDummyRequest({
    campus,
    owner,
    places: [
      place1,
    ],
  });

  const visitor = await createDummyVisitor({
    request,
    firstname: 'Foo',
    birthLastname: 'Bar',
    usageLastname: 'Bar',
    birthday: new Date('1970-01-01'),
    birthdayPlace: 'Paris',
  });
  request.stateMutation(EVENT_CREATE);
  await request.save();

  try {
    const { data: { mutateCampus: { mutateRequest: { validateVisitorStep } } } } = await mutatevalidateStepRequest(
      campus._id,
      request._id,
      visitor._id,
      {
        unit: unit1._id.toString(),
        role: roleCampusValidation.template,
      },
      WORKFLOW_DECISION_ACCEPTED,
      [],
      generateDummyAdmin(),
    );
    const dbVersion = await Visitor.findById(validateVisitorStep.id);
    const dbUnit = dbVersion.request.units.find((u) => u._id.equals(unit1._id));
    const dbStepOne = dbUnit.workflow.steps.find((s) => s._id.equals(unit1.workflow.steps[0]._id));
    expect(dbStepOne.state.value).toEqual(WORKFLOW_DECISION_ACCEPTED);
    const dbStepTwo = dbUnit.workflow.steps.find((s) => s._id.equals(unit1.workflow.steps[1]._id));
    expect(dbStepTwo.state.value).toEqual(WORKFLOW_DECISION_POSITIVE);
  } finally {
    await visitor.deleteOne();
    await request.deleteOne();
    await campus.deleteOne();
  }
});
