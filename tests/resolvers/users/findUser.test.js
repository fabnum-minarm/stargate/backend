import { nanoid } from 'nanoid';
import queryFactory, { gql } from '../../helpers/apollo-query';
import User, { createDummyUser } from '../../models/user';

function queryFindUser(email, userRole = null) {
  const { mutate } = queryFactory(userRole);
  return mutate({
    query: gql`
        query FindUserQuery($email: EmailAddress!) {
          findUser(email: $email) {
            id
            firstname
            lastname
            email {
                original
            }
          }
        }
    `,
    variables: { email },
  });
}

it('Test User not found with fake email', async () => {
  const fakeEmail = `${nanoid()}@localhost`;

  const { data: { findUser } } = await queryFindUser(fakeEmail);
  // no result
  expect(findUser).toBeNull();
});

it('Test to find a user by email', async () => {
  const dummyUser = await createDummyUser();

  try {
    const { errors } = await queryFindUser(dummyUser.email.original);
    // You're not authorized to find user while without rights
    expect(errors).toHaveLength(1);
    expect(errors[0].message).toContain('Not Authorised');
  } finally {
    await User.findOneAndDelete({ _id: dummyUser._id });
  }
});
