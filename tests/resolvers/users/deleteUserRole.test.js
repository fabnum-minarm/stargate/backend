import mongoose from 'mongoose';
import { nanoid } from 'nanoid';
import queryFactory, { gql } from '../../helpers/apollo-query';
import User, { createDummyUser, generateDummySuperAdmin } from '../../models/user';
import Unit, { createDummyUnit } from '../../models/unit';
import { generateDummyCampus } from '../../models/campus';
import {
  ROLE_BASIC_UNIT_VALIDATION,
  ROLE_BASIC_UNIT_VALIDATION_TEMPLATE,
  ROLE_FINAL_VALIDATION,
} from '../../../src/models/constants/role';
import generateRoleFromTemplate from '../../helpers/generateRoleFromTemplate';

function mutateDeleteUserRole(roleData, id, userRole = null) {
  const { mutate } = queryFactory(userRole);
  return mutate({
    mutation: gql`
        mutation DeleteUserRoleMutation($roleData: UserRoleInput! ,$id: ObjectID!) {
            deleteUserRole(roleData: $roleData, id: $id) {
                id
            }
        }
    `,
    variables: { roleData, id: id.toString() },
  });
}

it('Test to delete an user role', async () => {
  const unit1 = await createDummyUnit();
  const unit2 = await createDummyUnit();
  const campus = generateDummyCampus({ _id: nanoid() });
  const user = await createDummyUser({
    roles: [{
      role: generateRoleFromTemplate(ROLE_BASIC_UNIT_VALIDATION_TEMPLATE),
      units: [unit1, unit2],
      campuses: [campus],
    }],
  });
  const fakeId = mongoose.Types.ObjectId();

  try {
    {
      const { errors } = await mutateDeleteUserRole({ role: ROLE_FINAL_VALIDATION }, user._id);

      // You're not authorized to delete unit while without rights
      expect(errors).toHaveLength(1);
      expect(errors[0].message).toContain('Not Authorised!');
    }
    {
      const { errors } = await mutateDeleteUserRole(
        { role: ROLE_FINAL_VALIDATION },
        fakeId,
        generateDummySuperAdmin(),
      );
      // Found no unit with this id
      expect(errors).toHaveLength(1);
      expect(errors[0].message).toContain('User not found');
    }
    {
      const { data } = await mutateDeleteUserRole(
        { role: ROLE_BASIC_UNIT_VALIDATION, unit: { id: unit1._id.toString() }, campuses: [{ id: campus._id }] },
        user._id,
        generateDummySuperAdmin(),
      );
      expect(data.deleteUserRole).toHaveProperty('id', user.id);
      const dbVersion = await User.findOne({ _id: user._id });
      expect(dbVersion.roles).toHaveLength(1);
    }
    {
      const { data } = await mutateDeleteUserRole(
        { role: ROLE_BASIC_UNIT_VALIDATION, unit: { id: unit2._id.toString() }, campuses: [{ id: campus._id }] },
        user._id,
        generateDummySuperAdmin(),
      );
      expect(data.deleteUserRole).toHaveProperty('id', user.id);
      const dbVersion = await User.findOne({ _id: user._id });
      expect(dbVersion.roles).toHaveLength(0);
    }
  } finally {
    await User.findOneAndDelete(({ _id: user._id }));
    await Unit.findOneAndDelete({ _id: unit1._id });
    await Unit.findOneAndDelete({ _id: unit2._id });
  }
});
