import mongoose from 'mongoose';
import queryFactory, { gql } from '../../helpers/apollo-query';
import User, { createDummyUser, generateDummySuperAdmin } from '../../models/user';
import Unit, { createDummyUnit } from '../../models/unit';
import Campus, { createDummyCampus } from '../../models/campus';
import { ROLE_BASIC_USER_TEMPLATE } from '../../../src/models/constants/role';
import generateRoleFromTemplate from '../../helpers/generateRoleFromTemplate';

function mutateAddUserRole(roleData, id, campusId, userRole = null) {
  const { mutate } = queryFactory(userRole);
  return mutate({
    mutation: gql`
        mutation AddUserRoleMutation($roleData: UserRoleInput! ,$id: ObjectID!, $campusId: String!) {
            addUserRole(roleData: $roleData, id: $id, campusId: $campusId) {
                id
            }
        }
    `,
    variables: { roleData, id: id.toString(), campusId },
  });
}

it('Test to add an user role', async () => {
  const unit1 = await createDummyUnit();
  const unit2 = await createDummyUnit();
  const user = await createDummyUser();
  const role = generateRoleFromTemplate(ROLE_BASIC_USER_TEMPLATE);
  const campus = await createDummyCampus({ roles: [role] });
  const fakeId = mongoose.Types.ObjectId();

  const roleData = { role: role.template, campuses: [{ id: campus._id.toString(), label: campus.label }] };

  try {
    {
      const { errors } = await mutateAddUserRole(
        { ...roleData, unit: { id: unit1._id.toString(), label: unit1.label } },
        user._id,
        campus._id,
      );

      // You're not authorized to update user role while without rights
      expect(errors).toHaveLength(1);
      expect(errors[0].message).toContain('Not Authorised!');
    }
    {
      const { errors } = await mutateAddUserRole(
        { ...roleData, unit: { id: unit1._id.toString(), label: unit1.label } },
        fakeId,
        campus._id,
        generateDummySuperAdmin(),
      );
      // Found no user with this id
      expect(errors).toHaveLength(1);
      expect(errors[0].message).toContain('User not found');
    }
    {
      const { data } = await mutateAddUserRole(
        { ...roleData, unit: { id: unit1._id.toString(), label: unit1.label } },
        user._id,
        campus._id,
        generateDummySuperAdmin(),
      );
      expect(data.addUserRole).toHaveProperty('id', user.id);
      const dbVersion = await User.findOne({ _id: user._id });
      expect(dbVersion.roles).toHaveLength(1);
    }
    {
      const { data } = await mutateAddUserRole(
        { ...roleData, unit: { id: unit2._id.toString(), label: unit2.label } },
        user._id,
        campus._id,
        generateDummySuperAdmin(),
      );
      expect(data.addUserRole).toHaveProperty('id', user.id);
      const dbVersion = await User.findOne({ _id: user._id });
      expect(dbVersion.roles[0].units).toHaveLength(2);
    }
  } finally {
    await User.findOneAndDelete({ _id: user._id });
    await Unit.findOneAndDelete({ _id: unit1._id });
    await Unit.findOneAndDelete({ _id: unit2._id });
    await Campus.findOneAndDelete({ _id: campus._id });
  }
});
