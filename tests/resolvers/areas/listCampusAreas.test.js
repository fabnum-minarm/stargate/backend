import queryFactory, { gql } from '../../helpers/apollo-query';
import { generateDummySuperAdmin } from '../../models/user';
import Area, { createDummyArea } from '../../models/area';
import { generateDummyCampus } from '../../models/campus';

function queryListCampusAreas(campusId, user = null) {
  const { mutate } = queryFactory(user);
  return mutate({
    query: gql`
            query queryListCampusAreas($campusId: String!) {
                listCampusAreas(campusId: $campusId) {
                    list {
                        id
                        label
                        campuses {
                            id
                        }
                    }
                    meta {
                        total
                    }
                }
            }
        `,
    variables: { campusId },
  });
}

it('Test to list areas for a campus', async () => {
  const dummyCampus = generateDummyCampus();
  const areasWithCampus = await Promise.all(Array.from({ length: 2 })
    .map(() => createDummyArea({ campuses: [dummyCampus] })));
  await createDummyArea();
  try {
    {
      const { errors } = await queryListCampusAreas(dummyCampus._id);
      // You're not authorized to list areas while without rights
      expect(errors).toHaveLength(1);
      expect(errors[0].message).toContain('Not Authorised');
    }

    {
      const { data } = await queryListCampusAreas(dummyCampus._id, generateDummySuperAdmin());
      expect(data.listCampusAreas.meta.total).toBe(areasWithCampus.length);
    }
  } finally {
    await Area.deleteMany();
  }
});
