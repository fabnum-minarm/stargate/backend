import queryFactory, { gql } from '../../helpers/apollo-query';
import { generateDummySuperAdmin } from '../../models/user';
import Area, { createDummyArea } from '../../models/area';

function queryListArea(user = null) {
  const { mutate } = queryFactory(user);
  return mutate({
    query: gql`
            query queryListArea {
                listAreas {
                    list {
                        id
                        label
                        campuses {
                            id
                        }
                    }
                }
            }
        `,
  });
}

it('Test to list areas', async () => {
  await Promise.all(Array.from({ length: 5 }).map(() => createDummyArea()));
  try {
    {
      const { errors } = await queryListArea();
      // You're not authorized to list areas while without rights
      expect(errors).toHaveLength(1);
      expect(errors[0].message).toContain('Not Authorised');
    }

    {
      const { data } = await queryListArea(generateDummySuperAdmin());
      expect(data.listAreas.list).toHaveLength(5);
    }
  } finally {
    await Area.deleteMany();
  }
});
