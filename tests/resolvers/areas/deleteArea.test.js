import mongoose from 'mongoose';
import queryFactory, { gql } from '../../helpers/apollo-query';
import { generateDummySuperAdmin } from '../../models/user';
import Area, { createDummyArea } from '../../models/area';

function mutateDeleteArea(id, user = null) {
  const { mutate } = queryFactory(user);
  return mutate({
    mutation: gql`
            mutation DeleteAreaMutation($id: ObjectID!) {
                    deleteArea(id: $id) {
                        id
                        label
                    }
            }
        `,
    variables: { id: id.toString() },
  });
}

it('Test to delete an area', async () => {
  const dummyArea = await createDummyArea();
  try {
    {
      const { errors } = await mutateDeleteArea(dummyArea._id);

      // You're not authorized to delete area while without rights
      expect(errors).toHaveLength(1);
      expect(errors[0].message).toContain('Not Authorised!');
    }
    {
      const { errors } = await mutateDeleteArea(
        new mongoose.Types.ObjectId().toString(),
        generateDummySuperAdmin(),
      );
      expect(errors).toHaveLength(1);
      expect(errors[0].message).toContain('Area not found');
    }
    {
      const { data } = await mutateDeleteArea(
        dummyArea._id,
        generateDummySuperAdmin(),
      );
      expect(data.deleteArea).toHaveProperty('id', dummyArea.id);
      const dbVersion = await Area.findOne({ _id: dummyArea._id });
      expect(dbVersion).toBeNull();
    }
  } finally {
    await Area.findOneAndDelete({ _id: dummyArea._id });
  }
});
