import queryFactory, { gql } from '../../helpers/apollo-query';
import { generateDummySuperAdmin } from '../../models/user';
import Area, { generateDummyArea } from '../../models/area';
import { createDummyCampus } from '../../models/campus';

function mutateCreateArea(area, user = null) {
  const { mutate } = queryFactory(user);
  return mutate({
    mutation: gql`
            mutation CreateAreaMutation($area: AreaInput!) {
                createArea(area: $area) {
                    id
                    label
                    campuses {
                        id
                    }
                }
            }
        `,
    variables: { area },
  });
}

it('Test to create an area', async () => {
  const dummyCampus = await createDummyCampus();
  const dummyArea = generateDummyArea({ campuses: [dummyCampus] });
  try {
    {
      const { errors } = await mutateCreateArea(
        { label: dummyArea.label },
      );

      // You're not authorized to create area while without rights
      expect(errors).toHaveLength(1);
      expect(errors[0].message).toContain('Not Authorised');
    }

    {
      const { data } = await mutateCreateArea(
        { label: dummyArea.label, campuses: [dummyCampus._id] },
        generateDummySuperAdmin(),
      );
      expect(data.createArea).toHaveProperty('label', dummyArea.label);
      expect(data.createArea.campuses[0]).toHaveProperty('id', dummyCampus._id);
      const dbVersion = await Area.findOne(dummyArea);
      expect(dbVersion).toHaveProperty('__v', 0);
    }
  } finally {
    await Area.findOneAndDelete(dummyArea);
  }
});
