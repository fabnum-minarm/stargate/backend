import { nanoid } from 'nanoid';
import mongoose from 'mongoose';
import queryFactory, { gql } from '../../helpers/apollo-query';
import User, { createDummyUser, generateDummySuperAdmin } from '../../models/user';
import Campus, { createDummyCampus } from '../../models/campus';
import Area, { createDummyArea } from '../../models/area';
import { ROLE_HOST_TEMPLATE } from '../../../src/models/constants/role';
import generateRoleFromTemplate from '../../helpers/generateRoleFromTemplate';

function mutateEditArea(id, area, user = null) {
  const { mutate } = queryFactory(user);
  return mutate({
    mutation: gql`
            mutation EditAreaMutation($id: ObjectID!, $area: AreaInput!) {
                editArea(id: $id, area: $area) {
                    id
                    label
                    campuses {
                      id
                    }
                }
            }
        `,
    variables: { id, area },
  });
}

it('Test to edit an area', async () => {
  const dummyCampus = await createDummyCampus();
  const dummyArea = await createDummyArea();
  const dummyUser = await createDummyUser({
    roles: [{
      role: generateRoleFromTemplate(ROLE_HOST_TEMPLATE),
      area: dummyArea,
    }],
  });
  const newLabel = nanoid();
  try {
    {
      const { errors } = await mutateEditArea(dummyArea._id.toString(), { label: newLabel });
      // You're not authorized to edit area while without rights
      expect(errors).toHaveLength(1);
      expect(errors[0].message).toContain('Not Authorised');
    }

    {
      const { errors } = await mutateEditArea(
        new mongoose.Types.ObjectId().toString(),
        { label: newLabel }, generateDummySuperAdmin(),
      );

      expect(errors).toHaveLength(1);
      expect(errors[0].message).toContain('Area not found');
    }

    {
      const { data } = await mutateEditArea(
        dummyArea._id.toString(),
        { label: newLabel, campuses: [dummyCampus._id] },
        generateDummySuperAdmin(),
      );
      expect(data.editArea).toHaveProperty('label', newLabel);
      expect(data.editArea.campuses[0]).toHaveProperty('id', dummyCampus._id);
      const dbVersion = await Area.findOne({ _id: dummyArea._id });
      expect(dbVersion).toHaveProperty('__v', 1);
      const dbUser = await User.findById(dummyUser._id);
      expect(dbUser.roles[0].area).toHaveProperty('label', newLabel);
    }
  } finally {
    await Campus.findOneAndDelete({ _id: dummyCampus._id });
    await Area.findOneAndDelete({ _id: dummyArea._id });
    await User.findOneAndDelete({ _id: dummyUser._id });
  }
});
