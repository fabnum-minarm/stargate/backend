import { nanoid } from 'nanoid';

const generateRoleFromTemplate = (template) => ({
  label: nanoid(),
  shortLabel: nanoid(),
  ...template,
});

export default generateRoleFromTemplate;
