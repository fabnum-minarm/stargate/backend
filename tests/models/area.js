import { nanoid } from 'nanoid';
import Area from '../../src/models/area';

export const generateDummyArea = (...params) => ({
  label: nanoid(),
  ...params.reduce((acc, cur) => ({ ...acc, ...cur }), {}),
});

export const createDummyArea = async (...params) => {
  const dummyArea = generateDummyArea(...params);
  return Area.create(dummyArea);
};

export default Area;
